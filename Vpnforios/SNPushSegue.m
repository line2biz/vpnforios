//
//  SNPushSegue.m
//  Vpnforios
//
//  Created by Andrey Serebryakov on 13.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNPushSegue.h"

@implementation SNPushSegue

- (void)perform {
    if (DEVICE_IS_IPHONE) {
        UINavigationController *navigationViewController = (UINavigationController *)((UIViewController *)self.sourceViewController).parentViewController;
        [navigationViewController pushViewController:self.destinationViewController animated:YES];
    } else {
        
        UISplitViewController *splitViewController = (UISplitViewController *)((UINavigationController *)self.sourceViewController).parentViewController.parentViewController;
        
        if ([self.destinationViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *destinationNavigationController = (UINavigationController *)self.destinationViewController;
            UIViewController *destinationView = (UIViewController *)destinationNavigationController.childViewControllers[0];
            
            splitViewController.viewControllers = @[splitViewController.viewControllers[0], destinationNavigationController];
            
            [splitViewController performSelector:@selector(setDelegate:) withObject:destinationView];
            
            
            destinationView.navigationItem.leftBarButtonItem = nil;
        } else {
            UIViewController *destinationViewController = (UIViewController *)self.destinationViewController;
            UINavigationController *destinationNavigationController = [[UINavigationController alloc] initWithRootViewController:destinationViewController];
            
            splitViewController.viewControllers = @[splitViewController.viewControllers[0], destinationNavigationController];
            
            [splitViewController performSelector:@selector(setDelegate:) withObject:destinationViewController];
            
        }
        
    }
}

@end
