//
//  SNQuestionController.h
//  Vpnforios
//
//  Created by Andrey Serebryakov on 13.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMQuestion;

@interface SNQuestionController : UITableViewController
@property (strong, nonatomic) DMQuestion *question;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@end
