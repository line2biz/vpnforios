//
//  DMMessage+Category.m
//  Vpnforios
//
//  Created by Andrey Serebryakov on 08.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMMessage+Category.h"

#import "DMManager.h"

@implementation DMMessage (Category)

+ (DMMessage *)messageWithIdentifier:(NSNumber *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMMessage class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K = %@", @"identifier", identifier];
    fetchRequest.fetchLimit = 1;
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    
    return [results firstObject];
}

+ (void)parseResponseArray:(NSArray *)entities inContext:(NSManagedObjectContext *)context {
    NSManagedObjectContext *nestedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    nestedContext.parentContext = context;
    [nestedContext performBlock:^{
        for (NSDictionary *entity in entities) {
            NSInteger identifier = [entity[@"id"] integerValue];
            DMMessage *message = [DMMessage messageWithIdentifier:@(identifier)
                                                        inContext:context];
            // if message doesn't exists
            if (!message) {
                message = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMMessage class])
                                                        inManagedObjectContext:nestedContext];
                message.identifier = identifier;
            }
            
            message.title = entity[@"title"];
            message.brief = entity[@"short_description"];
            message.body  = entity[@"text"];
            message.isUnread = !((BOOL)[entity[@"read"] integerValue]);
            message.createdAt = [entity[@"create_time"] integerValue];
            message.imageURL = entity[@"image"];
            
            [nestedContext save:nil];
        }
    }];
    [context save:nil];
}

+ (void)removeAll {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMMessage class])];
    fetchRequest.predicate = nil;
    
    NSManagedObjectContext *context = [[DMManager sharedManager] defaultContext];
    
    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    for (DMMessage *message in results) {
        // remove message from data-model
        [context deleteObject:message];
    }
    [context save:nil];
    
    [[SNAppDelegate sharedDelegate] setBadgeNumber:0];
}

+ (NSUInteger)unreadMessagesCount {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMMessage class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"isUnread == 1"];
    
    NSUInteger count = [[[DMManager sharedManager] defaultContext] countForFetchRequest:fetchRequest error:NULL];
    return count;
}

@end
