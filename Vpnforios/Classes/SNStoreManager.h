//
//  SNStoreManager.h
//  Vpnforios
//
//  Created by Шурик on 30.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNStoreManager : NSObject

+ (SNStoreManager *)sharedManager;

@end
