//
//  SNDeviceDetailController.m
//  Vpnforios
//
//  Created by Admin on 11/28/14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDeviceDetailController.h"

@interface SNDeviceDetailController ()

@end

@implementation SNDeviceDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = [_device objectForKey:@"name"];
    self.labelId.text = [_device objectForKey:@"id"];
    self.labelDevice.text = [_device objectForKey:@"device"];
    self.labelName.text = [_device objectForKey:@"name"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
