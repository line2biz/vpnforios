//
//  SNSecureStatusController.m
//  Vpnforios
//
//  Created by Andrey Serebryakov on 15.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNSecureStatusController.h"

#import "SNInstallController.h"

@interface SNSecureStatusController ()

@property (nonatomic) NSUInteger row;

@end

@implementation SNSecureStatusController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Secure Status", nil);
    
//    switch ([DMUser defaultUser].secureStatus)
//    {
//        case SNSecureStatusProtected:
//            self.row = 2;
//            break;
//            
//        case SNSecureStatusNone:
//            self.row = 0;
//            break;
//            
//        case SNSecureStatusExpired:
//            self.row = 1;
//            break;
//    }
}

#pragma mark - UITableViewDataSource Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [super tableView:tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.row inSection:0]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [super tableView:tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:self.row inSection:0]];
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonProtect:(id)sender {
    UIViewController *installController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNInstallController class])];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:installController];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:navController animated:YES completion:NULL];
}

@end
