//
//  SNFreeQuotasController.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNFreeQuotasController.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNAddresBookController.h"

#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface SNFreeQuotasController ()

@property (weak, nonatomic) IBOutlet UILabel *firstTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *firstSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *firstButton;

@property (weak, nonatomic) IBOutlet UILabel *secondTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *secondButton;

@property (weak, nonatomic) IBOutletCollection(UILabel) NSArray *titleLabels;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *subtitles;
@property (weak, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@end

@implementation SNFreeQuotasController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    
    
    [self configureView];
    
    for (UILabel *label in self.titleLabels) {
        label.font = [UIFont myriadBoldFontWithSize:label.font.pointSize];
    }
    
    for (UILabel *label in self.subtitles) {
        label.font = [UIFont myriadFontWithSize:label.font.pointSize];
    }
    
    for (UIButton *button in self.buttons) {
        button.titleLabel.font = [UIFont myriadFontWithSize:button.titleLabel.font.pointSize];
    }
    
}

#pragma mark - Private Methods
- (void)configureView
{
    self.view.tintColor = [[SNAppDelegate sharedDelegate] tintColor];
    self.secondSubtitleLabel.textColor = self.view.tintColor;
    self.secondTitleLabel.textColor = self.view.tintColor;
}

#pragma mark - Oulet Methods
- (IBAction)didTapButtonRecommenViaEmail:(id)sender {
    SNAddresBookController *controller =  [[SNAddresBookController alloc] initWithSendType:SNAddressBookSendTypeEmail];
    controller.navigationItem.title = NSLocalizedString(@"Address Book", nil);
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:NULL];
}

- (IBAction)didTapButtonCheckOutFacebook:(id)sender {
    
    ACAccountStore *store = [[ACAccountStore alloc]init];
    ACAccountType *facebookAccount = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSDictionary *FacebookOptionsInitial = @{ACFacebookAppIdKey: @"886598114697746", ACFacebookPermissionsKey: @[@"email"],ACFacebookAudienceKey:ACFacebookAudienceFriends};
    [store requestAccessToAccountsWithType:facebookAccount options:FacebookOptionsInitial completion:^(BOOL granted, NSError *error) {
        
    }];
    
    NSDictionary *FacebookOptions = @{ACFacebookAppIdKey: @"886598114697746", ACFacebookPermissionsKey: @[@"publish_actions"],ACFacebookAudienceKey:ACFacebookAudienceFriends};
    [store requestAccessToAccountsWithType:facebookAccount options:FacebookOptions completion:^(BOOL granted, NSError *error) {
        if (granted)
        {
            NSArray *accounts = [store accountsWithAccountType:facebookAccount];
            if ([accounts count]>0) {
                ACAccount *facebookAccount = [accounts lastObject];
///TODO: Specifay the Facebook post content for free quotas
                NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               @"Vpnforios test", @"name",
                                               @"Build great social apps and get more installs.", @"caption",
                                               @"Allow your users to share stories on Facebook from your app using the iOS SDK.", @"description",
                                               @"https://vpnforios.test", @"link",
                                               @"http://i.imgur.com/g3Qc1HN.png", @"picture",
                                               nil];
                SLRequest *facebookRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:[NSURL URLWithString:@"https://graph.facebook.com/v2.0/me/feed"] parameters:params];
                [facebookRequest setAccount:facebookAccount];
                [facebookRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
///TODO: Send data to server for free quotas to user
                }];
            }
        }else{
            NSLog(@"ERROR: %@", error);
        }
        
    }];
    
}


@end
