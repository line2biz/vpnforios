//
//  SNTransactionManager.m
//  Vpnforios
//
//  Created by Шурик on 06.10.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNTransactionManager.h"

#import "NSURLRequest+Customization.h"
#import "SNConnectionManager.h"

#define SHARED_SECRET_VALUE @"8b546fc4186241a2af71d95c77334bf7"

NSString * const SNTransactionManagerDidReceivePayment = @"SNTransactionManagerDidReceivePayment";

@import StoreKit;

@interface SNTransactionManager () <SKPaymentTransactionObserver>

@end

@implementation SNTransactionManager

+ (SNTransactionManager *)sharedManager {
    static SNTransactionManager *__sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedManager = [SNTransactionManager new];
    });
    return __sharedManager;
}

- (void)buyProduct:(SKProduct *)product {
    
    if ([SKPaymentQueue canMakePayments]) {
        SKPayment * payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
        
    } else {
        NSString *title = NSLocalizedString(@"Warning", nil);
        NSString *msg = NSLocalizedString(@"Purchases are disabled", nil);
        NSString *cancel = NSLocalizedString(@"Dismiss", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:cancel otherButtonTitles:nil];
        [alertView show];
        
    }
}

#pragma mark - Pyment Transaction Observer
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    NSLog(@"updatedTransactions...");
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}



- (void)checkReceipt:(NSData *)receipt {
    
    NSString *receiptBase64 = [receipt base64EncodedStringWithOptions:0];
    
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                  NULL,
                                                                                  (CFStringRef)receiptBase64,
                                                                                  NULL,
                                                                                  (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                  kCFStringEncodingUTF8 ));
    
    NSLog(@"%s %@", __FUNCTION__, receiptBase64);
    
    NSDictionary *params = @{ @"receipt" : encodedString };
    NSURLRequest *request = [NSURLRequest requestWithAction:@"billing.validateReceipt" params:params requestType:NSURLRequestTypePost];
    [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Dismiss", nil), nil];
            [alertView show];
        } else {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:SNTransactionManagerDidReceivePayment object:nil];
        }
    }];
    
    
//    NSDictionary *dictionary = @{@"receipt-data": receiptBase64, @"password": SHARED_SECRET_VALUE};
//    
//    NSError *error = nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
//    if (error) {
//        NSLog(@"%s error: %@", __FUNCTION__, [error localizedDescription]);
//        return;
//    }
//    
//#warning for sandbox receipt validation; replace "sandbox" with "buy" in production or you will receive
//    // error codes 21006 or 21007
//    NSURL *url = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"];
//    NSMutableURLRequest *requestSandbox = [[NSMutableURLRequest alloc] initWithURL:url];
//    [requestSandbox setHTTPMethod:@"POST"];
//    [requestSandbox setHTTPBody:jsonData];
//    
//    [NSURLConnection sendAsynchronousRequest:requestSandbox queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
//        NSLog(@"%@", dictionary);
//        
//    }];
    
    
}

#pragma mark - Transactiom methods
- (void)finishedTransaction:(SKPaymentTransaction *)transaction
{
    //    NSLog(@"%s transaction: %@", __FUNCTION__, transaction);
//    NSLog(@"%s %@", __FUNCTION__, [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]]);
    //    [self checkReceipt:transaction.transactionReceipt];
    [self checkReceipt:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]]];
    
    
    
    
    //    DMIssue *issue = [DMIssue issueByProductIdentifier:transaction.payment.productIdentifier];
    //    if (issue) {
    //        //        NSLog(@"%s %@", __FUNCTION__, transaction.payment.productIdentifier);
    //
    //        issue.receipt = [NSString base64StringFromData:transaction.transactionReceipt];
    //        [[DMManager sharedManager] saveContext];
    //    } else {
    //        // Perhaps this subscription
    //        if ([_subscriptionIdentifers containsObject:transaction.payment.productIdentifier]) {
    //            //            SKProduct *product = [self productByIdentifier:transaction.payment.productIdentifier];
    //            if (![_productsDictionary valueForKey:transaction.payment.productIdentifier]) {
    //                //                NSLog(@"%s It is necessary to add product: %@", __FUNCTION__, product.productIdentifier);
    //            } else {
    //
    //                //                NSLog(@"%s %@ %@", __FUNCTION__, transaction.payment.productIdentifier, [transaction transactionDate]);
    //                //                NSLog(@"WE ARE HERE");
    //
    //
    //                NSDate *date = [_lastSubscriptionTransactionDates valueForKey:transaction.payment.productIdentifier];
    //                if (date == nil || [date laterDate:transaction.transactionDate]) {
    //зщ
    //                    //                    NSLog(@"WE ARE HERE WE ARE HERE %@", transaction.transactionDate);
    //                    [_lastSubscriptionTransactionDates setValue:transaction.transactionDate forKey:transaction.payment.productIdentifier];
    //                    [self checkReceipt:transaction.transactionReceipt];
    //                }
    //            }
    //
    //        }
    //
    //
    //    }
    
    
    
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"completeTransaction...");
    
    [self finishedTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"restoreTransaction... %@", transaction.transactionDate);
    
    [self finishedTransaction:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[transaction.error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}


@end
