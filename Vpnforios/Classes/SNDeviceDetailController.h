//
//  SNDeviceDetailController.h
//  Vpnforios
//
//  Created by Admin on 11/28/14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNDeviceDetailController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelId;
@property (weak, nonatomic) IBOutlet UILabel *labelDevice;
@property (weak, nonatomic) IBOutlet UILabel *labelName;

@property (nonatomic) NSDictionary *device;

@end
