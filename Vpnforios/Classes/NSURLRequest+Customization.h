//
//  NSURLRequest+Customization.h
//  First
//
//  Created by Andrey Serebryakov on 01.09.14.
//  Copyright (c) 2014 Andrey Serebryakov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NSURLRequestType) {
    NSURLRequestTypePost,
    NSURLRequestTypeGet
};

@interface NSURLRequest (Customization)

+ (NSURLRequest*)requestWithAction:(NSString*)action;
+ (NSURLRequest*)requestWithAction:(NSString*)action params:(NSDictionary*)params requestType:(NSURLRequestType)requestType;

@end
