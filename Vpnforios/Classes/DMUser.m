//
//  DMUser.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMUser.h"
#import "DMMessage+Category.h"

#import <NetworkExtension/NetworkExtension.h>

NSString * const DMUserDefaultKey           = @"DMUserDefaultKey";
NSString * const DMUserSecureStatusKey      = @"secureStatus";
NSString * const DMUserEmailKey             = @"email";
NSString * const DMUserPasswordKey          = @"password";
NSString * const DMUserLocationKey          = @"location";
NSString * const DMUserRemainingQuotasKey   = @"remainingQuotas";
NSString * const DMUserRemainingDaysKey     = @"remainingDays";
NSString * const DMUserPermanentQuotasKey   = @"permanentQuotas";
NSString * const DMUserProtectionTypeKey    = @"protectionType";
NSString * const DMUserUidKey               = @"uid";
NSString * const DMUserAccessTokenKey       = @"accessToken";

NSUInteger const DMUserPasswordMindLength   = 6;


@implementation DMUser

#pragma mark - Coding Protocol
- (instancetype)init
{
    self = [super init];
    if (self) {
        _location = [DMLocation locationWithIdentifier:nil];
        _remainingQuotas = 1000000;
        _permanentQuotas = 4500000000;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
//        _secureStatus = [coder decodeIntegerForKey:DMUserSecureStatusKey];
        _email = [coder decodeObjectForKey:DMUserEmailKey];
        _password = [coder decodeObjectForKey:DMUserPasswordKey];
        NSString *locationID = [coder decodeObjectForKey:DMUserLocationKey];
        _location = [DMLocation locationWithIdentifier:locationID];
        _remainingDays = [coder decodeIntegerForKey:DMUserRemainingDaysKey];
        _remainingQuotas = [coder decodeInt64ForKey:DMUserRemainingQuotasKey];
        _permanentQuotas = [coder decodeInt64ForKey:DMUserPermanentQuotasKey];
        _protectionType = [coder decodeIntegerForKey:DMUserProtectionTypeKey];
        _uid = [coder decodeIntegerForKey:DMUserUidKey];
        _accessToken = [coder decodeObjectForKey:DMUserAccessTokenKey];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
//    [aCoder encodeInteger:_secureStatus forKey:DMUserSecureStatusKey];
    [aCoder encodeObject:_email forKey:DMUserEmailKey];
    [aCoder encodeObject:_password forKey:DMUserPasswordKey];
    [aCoder encodeObject:_location.identifier forKey:DMUserLocationKey];
    [aCoder encodeInteger:_remainingDays forKey:DMUserRemainingDaysKey];
    [aCoder encodeInt64:_remainingQuotas forKey:DMUserRemainingQuotasKey];
    [aCoder encodeInt64:_permanentQuotas forKey:DMUserPermanentQuotasKey];
    [aCoder encodeInteger:_protectionType forKey:DMUserProtectionTypeKey];
    [aCoder encodeInteger:_uid forKey:DMUserUidKey];
    [aCoder encodeObject:_accessToken forKey:DMUserAccessTokenKey];
    
}



#pragma mark - Public Methods

- (void)save
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:DMUserDefaultKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)clear {
    [self setAccessToken:nil];
    [self setUid:0];
    [self setEmail:nil];
    [self setPassword:nil];
    [self save];
    
    [DMMessage removeAll];
}

- (BOOL)isValid
{
    
    return (self.accessToken && (self.uid != 0));
}

#pragma mark Class Methods

+ (BOOL)validateEmail:(NSString*)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

//+ (NSString *)stringWithSecureStatus:(SNSecureStatus)secureStatus;
//{
//    switch (secureStatus) {
//        case SNSecureStatusProtected:
//            return NSLocalizedString(@"Protected", nil);
//            break;
//            
//        default:
//            return NSLocalizedString(@"Not protected", nil);
//            break;
//    }
//}

+ (NSString *)stringWithProtectionType:(SNProtectionType)protectionType
{
    if ([NEVPNManager sharedManager].onDemandEnabled) {
        return NSLocalizedString(@"Always On", nil);
    } else {
        return NSLocalizedString(@"Manual", nil);
        
    }

    
    switch (protectionType) {
        case SNProtectionTypeAlwaysOn:
            return NSLocalizedString(@"Always On", nil);
            break;
            
        case SNProtectionTypeManual:
            return NSLocalizedString(@"Manual", nil);
            break;
    }
}

+ (DMUser *)defaultUser
{
    static DMUser *user;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSData *notesData = [[NSUserDefaults standardUserDefaults] objectForKey:DMUserDefaultKey];
        user = [NSKeyedUnarchiver unarchiveObjectWithData:notesData];
        if (!user) {
            user = [[DMUser alloc] init];
        }
    });
    return user;
}

@end
