//
//  SNLocationsController.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNLocationsController.h"

#import "SNHeaderView.h"
#import "UIColor+Customization.h"
#import "SNInstallController.h"

@interface SNLocationsController ()

@property (strong, nonatomic) NSArray *items;

@end

@implementation SNLocationsController


#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Server Locations", nil);
    
    if (DEVICE_IS_IPHONE) {
        self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    }
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.items = [DMLocation locationsArray];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Select a Country", nil);
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return NSLocalizedString(@"Choose which location you\nwant to connect throuh.", nil);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    DMLocation *location = self.items[indexPath.row];
    cell.imageView.image = location.image;
    
    cell.textLabel.text = location.title;
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DMUser *user = [DMUser defaultUser];
    user.location = self.items[indexPath.row];
    
    UIViewController *installController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNInstallController class])];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:installController];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:navController animated:YES completion:NULL];
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Set the text color of our header/footer text.
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.tintColor;
    
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.tintColor;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    
}

@end
