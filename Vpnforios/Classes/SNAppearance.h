//
//  SNAppearance.h
//  Vpnforios
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SNAppearanceColor) {
    SNAppearanceColorRed,
    SNAppearanceColorBlue
};

@interface SNAppearance : NSObject

+ (void)customizeApprarance;

@end
