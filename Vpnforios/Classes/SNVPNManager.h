//
//  SNVPNManager.h
//  Vpnforios
//
//  Created by Denis on 14.03.15.
//  Copyright (c) 2015 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SNVPNManagerDelegate;

@interface SNVPNManager : NSObject

@property (strong, nonatomic) id <SNVPNManagerDelegate> delegate;
@property (assign, nonatomic) BOOL alwaysON;

+ (instancetype)sharedManager;

- (void)configureVPN_andStart:(BOOL)start;

@end


@protocol SNVPNManagerDelegate <NSObject>

@optional
/// error is non nil if cofiguration wasn`t saved, otherwise configuration was saved
- (void)VPNManager:(SNVPNManager *)manager savedConfigurationWithError:(NSError *)error;

/// error is non nil if connection wasn`t set, otherwise connection has been started
- (void)VPNManager:(SNVPNManager *)manager startedConnectionWithError:(NSError *)error;

@end