//
//  DMQuestion+Category.m
//  Vpnforios
//
//  Created by Шурик on 10.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMQuestion+Category.h"

@implementation DMQuestion (Category)

+ (DMQuestion *)questionWithIdentifier:(NSNumber *)identifier inContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMQuestion class])];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K = %@", @"identifier", identifier];
    fetchRequest.fetchLimit = 1;

    NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
    
    return [results firstObject];
}

+ (void)parseResponse:(NSDictionary *)entities inContext:(NSManagedObjectContext *)context
{
    NSManagedObjectContext *nestedContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    nestedContext.parentContext = context;
    [nestedContext performBlock:^{
        for (NSDictionary *entity in entities)
        {
            NSInteger identifier = [entity[@"id"] integerValue];
            DMQuestion *question = [DMQuestion questionWithIdentifier:@(identifier) inContext:nestedContext];
            // if doesn't exists
            if (!question) {
                question = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DMQuestion class])
                                                         inManagedObjectContext:nestedContext];
                question.identifier = identifier;
            }
            
            question.title = entity[@"title"];
            question.body  = entity[@"body"];
            question.date  = [entity[@"date"] integerValue];
            
            [nestedContext save:nil];
        }
    }];
    [context save:nil];
}

@end
