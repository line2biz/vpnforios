//
//  DMDataPlan.h
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, DMDataPlanType) {
    DMDataPlanTypeMonthly,
    DMDataPlanTypeNoExpiration
};

@interface DMDataPlan : NSObject

@property (strong, nonatomic) NSString *identifierId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *specification;
@property (strong, nonatomic) NSNumber *price;
@property (assign, nonatomic) DMDataPlanType planType;

+ (NSString *)stringWithPlanType:(DMDataPlanType)planType;
- (NSString *)stringFromPrice; 

@end
