//
//  SNEmptyCell.m
//  Vpnforios
//
//  Created by Шурик on 19.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNEmptyCell.h"

@implementation SNEmptyCell

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
