//
//  UIColor+Customization.h
//  Vpnforios
//
//  Created by Шурик on 08.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Customization)

+ (UIColor *)mainBackgroundColor;
+ (UIColor *)customDarkGrayColor;

@end
