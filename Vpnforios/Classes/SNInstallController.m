//
//  SNInstallController.m
//  Vpnforios
//
//  Created by Шурик on 11.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNInstallController.h"

#import "SNHeaderView.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "NSURLRequest+Customization.h"
#import "SNConnectionManager.h"

#import <SVProgressHUD/SVProgressHUD.h>
#import <NetworkExtension/NetworkExtension.h>
#import <Security/Security.h>

#import "SNVPNManager.h"

static NSString *serviceName = @"com.mycompany.myAppServiceName";

@interface SNInstallController () <SNVPNManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIButton *installButton;


@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@end

@implementation SNInstallController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self savePassword:[[DMUser defaultUser] password] forKey:@"password"];
//    [self savePassword:@"vpn" forKey:@"sharedSecret"];
    
//    NSString *str  = @"To enable VPN, the configuration profile must be installed on your device. Please press “Continue”, select “Install” and “Done” as displayed below.\n\nNote: If you enabled a passcode on your device you will be asked to enter it during the install procedure. It´s a authority check from Apple´s iOS not from me.\n\nAnd of course the app dosn´t filtered or moitored your traffic. That´s the normal iOS warning if you install such an profile.";
//
    
    
    self.clearsSelectionOnViewWillAppear = YES;
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    
    self.tableView.estimatedRowHeight = 100.f;
    
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:26];
    
    for (UILabel *label in self.labels) {
        [UIFont customizeLabel:label];
    }
    [UIFont customizeButton:self.installButton];
    
    self.view.tintColor = [[SNAppDelegate sharedDelegate] tintColor];
    self.descLabel.textColor = self.view.tintColor;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(dismissSelf)];
    
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    

}


#pragma mark - Controller Methods
//- (void)savePassword:(NSString *)password forKey:(NSString *)key
//{
//    NSData *data = [password dataUsingEncoding:NSUTF8StringEncoding]; // Data to save. It can be a string too.
//    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
//    [dict setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
//    NSData *encodedKey = [key dataUsingEncoding:NSUTF8StringEncoding];
//    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrGeneric];
//    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrAccount];
//    [dict setObject:serviceName forKey:(__bridge id)kSecAttrService];
//    [dict setObject:(__bridge id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];
//    [dict setObject:data forKey:(__bridge id)kSecValueData];
//    
//    // If the keychain item already exists, delete it:
//    CFDictionaryRef attributes = nil;
//    if (SecItemCopyMatching((__bridge CFDictionaryRef)dict, (CFTypeRef *)&attributes) == noErr) {
//        OSStatus errorcode = SecItemDelete((__bridge CFDictionaryRef)dict);
//        NSAssert(errorcode == noErr, @"Couldn't update the Keychain Item." );
//        
//        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dict, NULL);
//        if(errSecSuccess != status) {
//            NSLog(@"Unable add item! error:%d",(int)status);
//        }
//    } else {
//        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dict, NULL);
//        if(errSecSuccess != status) {
//            NSLog(@"Unable add item! error:%d",(int)status);
//        }
//    }
//    
//}
//
//- (NSData *)passwordForKey:(NSString *)key
//{
//    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
//    [dict setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
//    NSData *encodedKey = [key dataUsingEncoding:NSUTF8StringEncoding];
//    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrGeneric];
//    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrAccount];
//    [dict setObject:serviceName forKey:(__bridge id)kSecAttrService];
//    [dict setObject:(__bridge id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];
//    [dict setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
//    [dict setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnPersistentRef]; // The most important part
//    
//    CFTypeRef result = NULL;
//    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)dict,&result);
//    
//    if( status != errSecSuccess) {
//        NSLog(@"Unable to fetch item! error:%d",(int)status);
//        return nil;
//    }
//    
//    NSData *resultData = (__bridge NSData *)result; //Your data is ready
//    return resultData;
//}



#pragma mark - Outlet Methods
- (IBAction)didTapButtonInstallProfile:(id)sender {
    
    NSString *title = NSLocalizedString(@"Must-Read Before Auto Setup", nil);
    NSString *msg = NSLocalizedString(@"1. If you enabled passcode in iOS setting \"General -> Passcode Lock\", you will be asked to enter your 4-digits device passcode (NOT VPN password)!\n2. VPN has to be manually turned ON from iOS settings \"General -> Network -> VPN\", after setup is complete.\n3. Users in China: please turn VPN OFF before this setup.", nil);
    NSString *btn = NSLocalizedString(@"Launch Auto Setup!", nil);
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:btn, nil];
    [alertView show];
    
    
}

- (void)dismissSelf {
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Table View Delegate
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ([indexPath isEqual:[NSIndexPath indexPathForRow:0 inSection:0]]) {
//        return 200.f;
//    }
//    
//    return 44.f;
//}


#pragma mark - Alert View delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    DMUser *user = [DMUser defaultUser];
    
    if ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorBlue) {
        user.adBlock = NO;
        [user save];
    } else {
        user.adBlock = YES;
        [user save];
    }
    
    if (IOSVersion() >= 8) {
        
        [SVProgressHUD show];
        
        [[SNVPNManager sharedManager] setDelegate:self];
        [[SNVPNManager sharedManager] setAlwaysON:YES];
        [[SNVPNManager sharedManager] configureVPN_andStart:NO];
        
        
        
    } else {
        
        NSArray *localizations = [[NSBundle mainBundle] preferredLocalizations];
        NSDictionary *params = @{ @"user_id" : @(user.uid),
                                  @"adblock" : @(user.adBlock),
                                  @"location" : user.location.identifier,
                                  @"lang" : localizations[0],
                                  @"protection" : @(user.protectionType),
                                  @"access_token" : user.accessToken
                                  };
        
        
        NSURLRequest *request = [NSURLRequest requestWithAction:@"user.profileInstall" params:params  requestType:NSURLRequestTypePost];
        [SVProgressHUD show];
        
        
        
        [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
            [SVProgressHUD dismiss];
            
            if (error) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
                [alertView show];
            } else {
                NSString *sURL = [json valueForKeyPath:@"response.install_page"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sURL]];
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    }
}


- (void)vpnConnectionStatusChanged {
    NEVPNManager *manager = [NEVPNManager sharedManager];
    NSLog(@"%s Connection status: %zd", __FUNCTION__, manager.connection.status);
    
    if (manager.connection.status == NEVPNStatusDisconnected) {
        NSError *startError;
        BOOL flag = [[NEVPNManager sharedManager].connection startVPNTunnelAndReturnError:&startError];
        NSLog(@"%s FLAG :%zd", __FUNCTION__, flag);
        [SVProgressHUD dismiss];
        if (startError) {
            NSLog(@"Start VPN failed: [%@]", startError.localizedDescription);
            [[NSNotificationCenter defaultCenter] removeObserver:self];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        

    }
    else if (manager.connection.status == NEVPNStatusConnected) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
}

#pragma mark - SNVPNManager Delegate
- (void)VPNManager:(SNVPNManager *)manager savedConfigurationWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    if (!error) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



@end
