//
//  NSURLRequest+Customization.m
//  First
//
//  Created by Andrey Serebryakov on 01.09.14.
//  Copyright (c) 2014 Andrey Serebryakov. All rights reserved.
//

#import "NSURLRequest+Customization.h"
#import "SNAppDelegate.h"
#import "SNConnectionManager.h"
#import "DMUser.h"

@implementation NSURLRequest (Customization)

+ (NSURLRequest*)requestWithAction:(NSString*)action {
    return [self requestWithAction:action params:nil requestType:NSURLRequestTypeGet];
}

+ (NSURLRequest *)requestWithAction:(NSString *)action params:(NSDictionary *)params requestType:(NSURLRequestType)requestType {
    
    NSMutableDictionary *dictionary = nil;
    if ([[DMUser defaultUser] isValid]) {
        dictionary = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                     @"user_id": @([[DMUser defaultUser] uid]),
                                                                     @"access_token" : [[DMUser defaultUser] accessToken],
                                                                     @"device" : [[NSUserDefaults standardUserDefaults] stringForKey:SNAppDeviceTokenKey]
                                                                     }];
    } else {
        dictionary = [NSMutableDictionary new];
    }
    
    if (params) {
        [dictionary addEntriesFromDictionary:params];
    }
    
    [dictionary setObject:action forKey:@"action"];
    
    NSString *sLang = [[[NSLocale currentLocale] localeIdentifier] substringToIndex:2];
    if (![sLang isEqual:@"de"]) {
        sLang = @"en";
    }
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:[NSString stringWithFormat:@"lang=%@", sLang]];
    
    for (NSString *key in dictionary) {
        [paramsArray addObject:[NSString stringWithFormat:@"%@=%@", key, dictionary[key]]];
    }
    
    NSString *sParams = [paramsArray componentsJoinedByString:@"&"];
    
    
    NSString *sUrl = @"http://adm.vpnforios.com/api.php";
    
    NSMutableURLRequest *request = nil;
    if (requestType == NSURLRequestTypeGet) {
        sUrl = [sUrl stringByAppendingFormat:@"?%@", sParams];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:sUrl]];
        request.HTTPMethod = @"GET";
        
        
    } else {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:sUrl]];
        request.HTTPMethod = @"POST";
        request.HTTPBody = [sParams dataUsingEncoding:NSUTF8StringEncoding];
        
    }
    
    
    return (NSURLRequest *)request;
}

@end
