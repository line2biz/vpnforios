//
//  SNChangePasswordController.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNChangePasswordController.h"

#import "SNHeaderView.h"
#import "UIFont+Customization.h"
#import "UIColor+Customization.h"
#import "SVProgressHUD.h"
#import "SNConnectionManager.h"
#import "NSURLRequest+Customization.h"

@import ObjectiveC.runtime;

typedef void (^AlertViewBlock)(UIAlertView *alertView, NSInteger buttonIndex);
#define kALERT_VIEW_BLOCK  @"kALERT_VIEW_BLOCK"


@interface SNChangePasswordController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *originalLabel;
@property (weak, nonatomic) IBOutlet UILabel *newoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *confirmLabel;

@property (weak, nonatomic) IBOutlet UITextField *originalTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;

@property (nonatomic, readonly) DMUser *user;

@end

@implementation SNChangePasswordController

@dynamic user;

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (DEVICE_IS_IPHONE) {
        self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    }
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.clearsSelectionOnViewWillAppear = YES;
    
    self.navigationItem.title = NSLocalizedString(@"Change Password", nil);
    
    self.originalLabel.text = NSLocalizedString(@"Original", nil);
    self.newoneLabel.text = NSLocalizedString(@"New", nil);
    self.confirmLabel.text = NSLocalizedString(@"Confirm", nil);
    
    self.originalTextField.placeholder = NSLocalizedString(@"Enter current password", nil);
    self.passwordTextField.placeholder = NSLocalizedString(@"Enter new password", nil);
    self.confirmTextField.placeholder = NSLocalizedString(@"Confirm new password", nil);
    
    self.originalTextField.tag = 0;
    self.passwordTextField.tag = 1;
    self.confirmTextField.tag = 2;
    
    for (UILabel *label in self.labels) {
        label.font = [UIFont myriadFontWithSize:label.font.pointSize];
    }
    
    for (UITextField *textField in self.textFields) {
        textField.delegate = self;
        [textField addTarget:self action:@selector(editingChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    
    [self validateFields];
}

#pragma mark - Property Accessors
- (DMUser *)user
{
    return [DMUser defaultUser];
}


#pragma mark - Private Methods
- (void)configureView
{
    self.view.tintColor = [[SNAppDelegate sharedDelegate] tintColor];
}

- (BOOL)validateFields
{
    BOOL flag = NO;
    if ([self.originalTextField.text isEqualToString:self.user.password]) {
        NSString *password = [self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([password length] >= DMUserPasswordMindLength) {
            NSString *confirm = [self.confirmTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if ([password isEqualToString:confirm]) {
                flag = YES;
            }
            
        }
    }
    self.navigationItem.rightBarButtonItem.enabled = YES;
    return flag;
}

#pragma mark - Outlet Methods
- (void)editingChanged:(id)sender
{
    [self validateFields];
}

- (IBAction)didTapButtonCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didTapButtonSave:(id)sender
{
    [self updatePassword];
}

#pragma mark - Text Fields Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger tag = textField.tag + 1;
    [self.textFields enumerateObjectsUsingBlock:^(UITextField *obj, NSUInteger idx, BOOL *stop) {
        if (obj.tag == tag) {
            [obj becomeFirstResponder];
            *stop = YES;
        }
    }];
    
    if (textField == [self.textFields lastObject]) {
        [self updatePassword];
    }
    
    return YES;
}


#pragma mark - Server Methods
- (void)updatePassword {
    
    NSError *error = nil;
    NSString *newPassword = [self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *confirmPassword = [self.confirmTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *oldPassword = [self.originalTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    if ([newPassword length] == 0 || [confirmPassword length] == 0 || [oldPassword length] == 0) {
        NSString *title = NSLocalizedString(@"Change Password", nil);
        NSString *msg = NSLocalizedString(@"Enter your current password, new password, and confirm your new password.", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return;
    }
    
    
    if (![SNAppDelegate isPasswordValid:newPassword error:&error]) {
        
        __weak typeof(self) weakSelf = self;
        AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.passwordTextField becomeFirstResponder];
            }
        };
        
        NSString *title = NSLocalizedString(@"Change Password", nil);
        NSString *msg = [error localizedDescription];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
        [alertView show];
        
        return;
    }
    
    if (![confirmPassword isEqualToString:newPassword]) {
        __weak typeof(self) weakSelf = self;
        AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.confirmTextField becomeFirstResponder];
            }
        };
        
        NSString *title = NSLocalizedString(@"Change Password", nil);
        NSString *msg = NSLocalizedString(@"Password and confirmation does not match.", nil);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
        [alertView show];
        
        return;
        
    }
    
    
    
    
    NSDictionary *params = @{
                             @"current_password" : oldPassword,
                             @"new_password" : newPassword
                             };
    
    [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.change_password" params:params requestType:NSURLRequestTypePost]
                   completionHandler:^(NSDictionary *json, NSError *error) {
                       if (error) {
                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                           [alertView show];
                       } else {
                           
                           self.originalTextField.text = @"";
                           self.passwordTextField.text = @"";
                           self.confirmTextField.text = @"";
                           
                           if ([self.originalTextField isFirstResponder]) {
                               [self.originalTextField resignFirstResponder];
                           }
                           if ([self.passwordTextField isFirstResponder]) {
                               [self.passwordTextField resignFirstResponder];
                           }
                           if ([self.confirmTextField isFirstResponder]) {
                               [self.confirmTextField resignFirstResponder];
                           }
                           
                           
                           [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Password changed", nil)];
                           
                           // save changed data to local data-model
                           DMUser *user = [DMUser defaultUser];
                           user.password = newPassword;
                           user.accessToken = [json valueForKeyPath:@"response.data.access_token"];
                           [user save];
                           [self dismissViewControllerAnimated:YES completion:NULL];
                           
                       }
                   }];
}


#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    AlertViewBlock block = objc_getAssociatedObject(alertView, kALERT_VIEW_BLOCK);
    if (block) {
        block(alertView, buttonIndex);
    }
}

@end
