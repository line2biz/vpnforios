//
//  DMQuestion.h
//  Vpnforios
//
//  Created by Andrey Serebryakov on 10.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DMQuestion : NSManagedObject

@property (nonatomic) int64_t identifier;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * body;
@property (nonatomic) NSTimeInterval date;

@end
