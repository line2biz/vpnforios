//
//  SNDashboardController.m
//  Vpnforios
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDashboardController.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHeaderView.h"
#import "SNLoginController.h"
#import "SNSwitchedCell.h"
#import "SNLocationsController.h"
#import "SNProtectionsController.h"
#import "SNFreeQuotasController.h"
#import "SNChangePasswordController.h"
#import "SNDeleteAccountController.h"
#import "SNInstallController.h"
#import "SNSecureStatusController.h"
#import "SNDevicesController.h"
#import "SNStatisticsController.h"

#import "SNAppDelegate.h"

#import "NSURLRequest+Customization.h"
#import "SNConnectionManager.h"
#import "DMMessage+Category.h"
#import "DMManager.h"
#import "SNEmptyCell.h"
#import "SNTransactionManager.h"
#import "SNTempCell.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>

#import <SVProgressHUD/SVProgressHUD.h>
#import <NetworkExtension/NetworkExtension.h>

#import "SNVPNManager.h"

#define kTABLE_SECTION_HEIGHT 25.f
#define kROW_LEFT_MARGIN 15.f
#define LOCATION_INDEX_PATH [NSIndexPath indexPathForRow:1 inSection:0]


typedef NS_ENUM(NSInteger, SNDashoboadCellType) {
    SNDashoboadCellTypeSecureStatus,
    SNDashoboadCellTypeNotProtected,
    SNDashoboadCellTypeVPNProtection,
    SNDashoboadCellTypeAdBlock,
    SNDashoboadCellTypeLocation
    
};

typedef NS_ENUM(NSInteger, SNVPNStatus) {
    SNVPNStatusNone,
    SNVPNStatusDisconnected,
    SNVPNStatusConnected,
    SNVPNStatusExperied
};



@import SystemConfiguration;

@interface SNDashboardController () <SNAppDelegateForegroundProtocol, SNVPNManagerDelegate>

@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic, readonly) DMUser *user;
@property (nonatomic) BOOL hasPlan;
@property (strong, nonatomic) NSIndexPath *lastSelectedIndexPath;
@property (assign, nonatomic) BOOL isFirstRowSelected;
@property (strong, nonatomic) NSDictionary *planDictionary;

@property (strong, nonatomic) UISwitch *connectionSwitch;
@property (strong, nonatomic) UISwitch *addblockSwitch;

@end

@implementation SNDashboardController

@dynamic user;

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = YES;
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    
    [self.tableView registerClass:[SNEmptyCell class] forCellReuseIdentifier:@"EmptyCell"];
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    [self configureView];
    
    if (![self.user isValid]) {
        [self showLoginControllerAnimated:NO];
    }
    
    self.connectionSwitch = [UISwitch new];
    self.addblockSwitch = [UISwitch new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didGetUserLoggedInNotification) name:SNAppUserDidLogin object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didGetUserDeletedNotification) name:SNAppUserDidDeleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMessages) name:@"loadMessages" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadProfile) name:SNTransactionManagerDidReceivePayment object:nil];
    
    if (IOSVersion() >= 8) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vpnConnectionStatusChanged) name:NEVPNStatusDidChangeNotification object:nil];
    }
    
    
    
    
    [self registerObservers];
    
//    if ([self isVPNConnected]) {
//        NSLog(@"connected");
//    } else {
//        NSLog(@"Not connected");
//
//    }
    
    
    [self loadProfile];
    

    [self isVPNConnectedViaProxy];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
}


#pragma mark - Private Methods


#pragma mark VPN
- (void)loadMessages {
    // update Messages badge
    [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.news"] completionHandler:^(NSDictionary *json, NSError *error) {
        [DMMessage parseResponseArray:[json valueForKeyPath:@"response.news"] inContext:[[DMManager sharedManager] defaultContext]];
        NSString *unreadCount = [json valueForKeyPath:@"response.unread"];
        [[SNAppDelegate sharedDelegate] setBadgeNumber:[unreadCount integerValue]];
    }];
}

- (void)didGetUserLoggedInNotification {
    [self.tableView reloadData];
    [self registerObservers];
    [self loadProfile];
}

- (void)didGetUserDeletedNotification {
    [self showLoginControllerAnimated:YES];
}

- (void)dealloc
{
    [self unregisterObservers];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma mark - Private Methods
- (void)configureView
{
    self.view.tintColor = [[SNAppDelegate sharedDelegate] tintColor];
}

#pragma mark - Observer Methods
- (void)registerObservers
{
    [self.user addObserver:self forKeyPath:DMUserLocationKey        options:NSKeyValueObservingOptionNew context:NULL];
    [self.user addObserver:self forKeyPath:DMUserRemainingDaysKey   options:NSKeyValueObservingOptionNew context:NULL];
    [self.user addObserver:self forKeyPath:DMUserRemainingQuotasKey options:NSKeyValueObservingOptionNew context:NULL];
    [self.user addObserver:self forKeyPath:DMUserPermanentQuotasKey options:NSKeyValueObservingOptionNew context:NULL];
    [self.user addObserver:self forKeyPath:DMUserProtectionTypeKey  options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)unregisterObservers
{
    [self.user removeObserver:self forKeyPath:DMUserLocationKey];
    [self.user removeObserver:self forKeyPath:DMUserRemainingDaysKey];
    [self.user removeObserver:self forKeyPath:DMUserRemainingQuotasKey];
    [self.user removeObserver:self forKeyPath:DMUserPermanentQuotasKey];
    [self.user removeObserver:self forKeyPath:DMUserProtectionTypeKey];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqual:DMUserLocationKey])
        [self.tableView reloadData];
    else if ([keyPath isEqual:DMUserProtectionTypeKey])
        [self.tableView reloadData];
    else
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}


#pragma mark - Property Accessors
- (DMUser *)user
{
    return [DMUser defaultUser];
}

#pragma mark - Public Methods
- (void)showLoginControllerAnimated:(BOOL)animated
{
    [self.tableView reloadData];
    
    UIViewController *loginC = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNLoginController class])];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginC];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    
//    UIViewController *empty = [self.storyboard instantiateViewControllerWithIdentifier:@"SNMessageController"];
//    [SNAppDelegate sharedDelegate].window.rootViewController = empty;
    [self presentViewController:navController animated:animated completion:NULL];
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonLogout:(id)sender
{
    [self unregisterObservers];
    [[DMUser defaultUser] clear];
    
    [self showLoginControllerAnimated:YES];
}

- (void)didTapSwitchConnect:(UISwitch *)sender {

    if (sender.on) {
        
        NEVPNManager *manager = [NEVPNManager sharedManager];
        switch (manager.connection.status) {
            case NEVPNStatusInvalid: {
                UIViewController *installController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNInstallController class])];
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:installController];
                navController.modalPresentationStyle = UIModalPresentationFormSheet;
                
                [self presentViewController:navController animated:YES completion:NULL];
            }
                break;
                
            case NEVPNStatusDisconnected: {
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [[SNVPNManager sharedManager] setDelegate:self];
                [[SNVPNManager sharedManager] setAlwaysON:![[DMUser defaultUser] protectionType]];
                [[SNVPNManager sharedManager] configureVPN_andStart:YES];
            }
                break;
            default:
                break;
        }
        
        
        
    } else {
        [[SNVPNManager sharedManager] setAlwaysON:NO];
        [[SNVPNManager sharedManager] configureVPN_andStart:NO];
        [[NEVPNManager sharedManager].connection stopVPNTunnel];
        
    }
}



- (BOOL)isVPNConnectedViaProxy {
    
    return self.user.adBlock;
    
    NSDictionary *dict = CFBridgingRelease(CFNetworkCopySystemProxySettings());
    
    
    if ([dict valueForKey:@"HTTPSProxy"]) {
        return YES;
    } else {
        return NO;
    }
    
    return [dict count] > 0;
}

- (void)vpnConnectionStatusChanged {
    
    NEVPNStatus status = [NEVPNManager sharedManager].connection.status;
    
    if (status == NEVPNStatusConnecting || status == NEVPNStatusDisconnecting) {
        NSLog(@"vpnConnectionStatusChanged: connecting/disconnecting");
        return;
    }
    
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
    
    [self.tableView reloadData];
}

- (IBAction)didTapSwitchAdBlock:(UISwitch *)sender {
    
    if ([DMUser defaultUser].adBlock == sender.on) {
        return;
    }
    
    NSDictionary *params = @{ @"ad_block" : @(sender.isOn) };
    NSURLRequest *request = [NSURLRequest requestWithAction:@"user.setAdblock" params:params requestType:NSURLRequestTypePost];
    
    
    NEVPNManager *manager = [NEVPNManager sharedManager];
    BOOL restart = (manager.connection.status == NEVPNStatusConnected || manager.connection.status == NEVPNStatusConnecting);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NEVPNStatusDidChangeNotification object:nil];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    if (restart) {
        [manager.connection stopVPNTunnel];
    }
    
    
    [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vpnConnectionStatusChanged) name:NEVPNStatusDidChangeNotification object:nil];
        
        if (error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        } else {
            [[DMUser defaultUser] setAdBlock:[sender isOn]];
            NEVPNManager *manager = [NEVPNManager sharedManager];
            if (restart) {
                NSError *startError = nil;
                [manager.connection startVPNTunnelAndReturnError:&startError];
                if (startError) {
                    [SVProgressHUD showErrorWithStatus:[startError localizedDescription]];
                }
                
            } else {
                [SVProgressHUD dismiss];
            }
        }
        
        
    }];
    
    

    
}

- (IBAction)didTapButtonProtect:(id)sender {
    
    if (IOSVersion() >= 8) {
        NEVPNManager *manager = [NEVPNManager sharedManager];
        switch (manager.connection.status) {
            case NEVPNStatusInvalid: {
                UIViewController *installController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNInstallController class])];
                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:installController];
                navController.modalPresentationStyle = UIModalPresentationFormSheet;
                
                [self presentViewController:navController animated:YES completion:NULL];
            }
                break;
                
            case NEVPNStatusDisconnected: {
                NSError *startError = nil;
                BOOL flag = [[NEVPNManager sharedManager].connection startVPNTunnelAndReturnError:&startError];
                if (startError) {
                    NSLog(@"Start VPN failed: [%@]", startError.localizedDescription);
                }
            }
                break;
            default:
                break;
        }
    } else {
        UIViewController *installController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNInstallController class])];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:installController];
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:navController animated:YES completion:NULL];
    }
    
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:NSStringFromClass([SNDeleteAccountController class])]) {
        
    }
}
- (IBAction)didTapButtonRefresh:(id)sender {
    [self loadProfile];
}

#pragma mark - Application Protocol
- (void)applicationWillAppearInForeground {
    
    UITabBarController *tabBarController;
    
    if (DEVICE_IS_IPHONE) {
        tabBarController = (UITabBarController *)self.navigationController.parentViewController;
    } else {
        tabBarController = (UITabBarController *)[SNAppDelegate sharedDelegate].window.rootViewController;
    }
    
    tabBarController.selectedIndex = 0;
    
    [self loadProfile];
}

#pragma mark - Table View Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.user isValid]) {
        return 3;
    } else {
        return 0;
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        if ([self.planDictionary count] == 1) {
            return NSLocalizedString(@"Subscription Plan", nil);
        } else {
            return NSLocalizedString(@"Permanent Data (no expire date)", nil);
            
        }
    }
    else if (section == 2)
        return NSLocalizedString(@"Settings", nil);
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        if (IOSVersion() >= 8) {
            return 3 + ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorRed);
        }
        else  if ([self vnpStatus] == SNVPNStatusNone) {
            return 2;
        }
        else  {
            return 2 + ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorRed);
        }
    }
    else if (section == 1) {
        if (self.hasPlan) {
            return [self.planDictionary count];
        } else {
            return 1;
        }
    
    }
    else if (section == 2) {
        return 5;
    }
    return 0;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForType:(SNDashoboadCellType)cellType {
    UITableViewCell *cell = nil;
    NSString *imageName;
    switch (cellType) {
        case SNDashoboadCellTypeSecureStatus: {
            imageName = @"Icon-Secure";
            cell = [tableView dequeueReusableCellWithIdentifier:@"SecureCell"];
            cell.textLabel.text = NSLocalizedString(@"Secure Status", nil);
            
            if ([self vnpStatus] == SNVPNStatusConnected) {
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon-Dot-Green"]];
                cell.detailTextLabel.text = NSLocalizedString(@"Protected", nil);
            } else {
                cell.detailTextLabel.text = NSLocalizedString(@"Not protected", nil);
                cell.detailTextLabel.textColor = [[SNAppDelegate sharedDelegate] tintColor];
                cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon-Dot-Red"]];
            }
        }
            
            break;
        case SNDashoboadCellTypeNotProtected:
            break;
        case SNDashoboadCellTypeVPNProtection: {
            SNSwitchedCell *switchCell = [tableView dequeueReusableCellWithIdentifier:@"SwitchedCell"];
            
            NEVPNManager *manager = [NEVPNManager sharedManager];
            if (manager.connection.status == NEVPNStatusConnected) {
                switchCell.titleLabel.text = NSLocalizedString(@"VPN connection", nil);
                
            } else {
                switchCell.titleLabel.text = NSLocalizedString(@"VPN connection", nil);
            }
            
            
            switchCell.iconImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Block"];
            
            [switchCell.switchControl addTarget:self action:@selector(didTapSwitchConnect:) forControlEvents:UIControlEventValueChanged];
            switchCell.switchControl.on = (manager.connection.status == NEVPNStatusConnected);
            cell = switchCell;
        }
            break;
        
        case SNDashoboadCellTypeAdBlock: {
            SNSwitchedCell *switchCell = [tableView dequeueReusableCellWithIdentifier:@"SwitchedCell"];
            switchCell.titleLabel.text = NSLocalizedString(@"AdBlock", nil);
            switchCell.iconImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-AdBlock"];
            [switchCell.switchControl addTarget:self action:@selector(didTapSwitchAdBlock:) forControlEvents:UIControlEventValueChanged];
            
            switchCell.switchControl.on = [self isVPNConnectedViaProxy];
            cell = switchCell;
        }
            break;
            
        case SNDashoboadCellTypeLocation: {
            cell = [tableView dequeueReusableCellWithIdentifier:@"RightDetailedCell"];
            cell.textLabel.text = NSLocalizedString(@"Server Location", nil);
            cell.detailTextLabel.text = self.user.location.title;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            imageName = @"Icon-Location";
        }
        default:
            break;
    }
    
    if (imageName) {
        cell.imageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:imageName];
    } else {
        cell.imageView.image = nil;
    }
    return cell;
}

- (SNVPNStatus)vnpStatus {
    if (IOSVersion() >= 8) {
        NEVPNManager *manager = [NEVPNManager sharedManager];
        if (manager.connection.status == NEVPNStatusDisconnected) {
            return SNVPNStatusDisconnected;
        }
        else if (manager.connection.status == NEVPNStatusConnected) {
            return SNVPNStatusConnected;
        }
        else {
            return SNVPNStatusNone;
        }
        
    } else {
        NSDictionary *dict = CFBridgingRelease(CFNetworkCopySystemProxySettings());
        if ([dict valueForKey:@"HTTPEnable"]) {
            return SNVPNStatusConnected;
        } else {
            return SNVPNStatusNone;
        }
    }
    
    return SNVPNStatusNone;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    NSString *imageName = nil;
    
    if (indexPath.section == 0) {
        
        if (IOSVersion() >= 8) {
            if (indexPath.row == 0) {
                return [self tableView:tableView cellForType:SNDashoboadCellTypeVPNProtection];
            }
            
            else if (indexPath.row == 1) {
                if ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorRed) {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeAdBlock];
                } else {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeSecureStatus];
                }
            }
            
            else if (indexPath.row == 2) {
                if ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorRed) {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeSecureStatus];
                } else {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeLocation];
                }
            }
            else {
                return [self tableView:tableView cellForType:SNDashoboadCellTypeLocation];
            }
        
        }
        else if ([self vnpStatus] == SNVPNStatusNone) {
            if (indexPath.row == 0) {
                if (DEVICE_IS_IPHONE){
                    cell = [tableView dequeueReusableCellWithIdentifier:@"NotProtectedCell"];
                } else {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"ExpiredCell"];;
                }
                return cell;
            }
            else if (indexPath.row == 1) {
                return [self tableView:tableView cellForType:SNDashoboadCellTypeSecureStatus];
            }
            else {
                return [self tableView:tableView cellForType:SNDashoboadCellTypeSecureStatus];
            }
         
            
        }
        else {
            if (indexPath.row == 0) {
                if ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorRed) {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeAdBlock];
                } else {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeSecureStatus];
                }
            }
            else if (indexPath.row == 1) {
                if ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorRed) {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeSecureStatus];
                } else {
                    return [self tableView:tableView cellForType:SNDashoboadCellTypeLocation];
                }
            }
            else {
                return [self tableView:tableView cellForType:SNDashoboadCellTypeLocation];
            }
            
        }
        
    }
    
    else if (indexPath.section == 1) {
        
        
        if (self.hasPlan) {
            if ([self.planDictionary count] == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Permanent Quotes"];
                cell.textLabel.text = NSLocalizedString(@"Permanent Traffic", nil);
                
                long long permanentQuotas = [[self.planDictionary valueForKey:@"quota"] longLongValue];
                
                if (permanentQuotas == 0) {
                    cell.detailTextLabel.text = @"0.00 Mb";
                }
                else {
                    cell.detailTextLabel.text = [NSByteCountFormatter stringFromByteCount:permanentQuotas countStyle:NSByteCountFormatterCountStyleFile];
                }
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.accessoryType = UITableViewCellAccessoryNone;
                imageName = @"Icon-Quota";

            } else {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Permanent Traffic"];
                    cell.textLabel.text = NSLocalizedString(@"Remaining Traffic", nil);
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    NSTimeInterval timeInterval =  [[self.planDictionary valueForKey:@"expiration"] doubleValue];
                    
                    NSUInteger unitFlags = NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute;
                    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                    NSDateComponents *components = [calendar components:unitFlags fromDate:[NSDate date] toDate:[NSDate dateWithTimeIntervalSince1970:timeInterval] options:0];
                    
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%zd Day(s) %zdh %zdmin", components.day, components.hour, components.minute];
                    
                    
                    imageName = @"Icon-Remaining";

                    
                } else {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Permanent Quotes"];
                    cell.textLabel.text = NSLocalizedString(@"Permanent Quotas", nil);
                    
                    long long permanentQuotas = [[self.planDictionary valueForKey:@"quota"] longLongValue];
                    
                    if (permanentQuotas == 0) {
                        cell.detailTextLabel.text = @"0.00 Mb";
                    }
                    else {
                        cell.detailTextLabel.text = [NSByteCountFormatter stringFromByteCount:permanentQuotas countStyle:NSByteCountFormatterCountStyleFile];
                    }
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    imageName = @"Icon-Quota";
                }
                
            }
        }
        else {
            NSLog(@"%s EMPTY CELL", __FUNCTION__);
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCell"];
            cell.textLabel.text = NSLocalizedString(@"", nil);
            return cell;
        }
    }
    
    else if (indexPath.section == 2) {
        NSUInteger rowIndex = indexPath.row;
        if (rowIndex == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BacisCell"];
            cell.textLabel.text = NSLocalizedString(@"Get Free extra Quoats", nil);
            cell.detailTextLabel.text = nil;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            imageName = @"Icon-Start";
        }
        
        //    DEVICES
        else if (rowIndex == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BacisCell"];
            cell.textLabel.text = NSLocalizedString(@"My Devices", nil);
            cell.detailTextLabel.text = nil;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            imageName = @"Icon-Devices";
        }
        
        // Statistic
        else if (rowIndex == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BacisCell"];
            cell.textLabel.text = NSLocalizedString(@"Statistics", nil);
            cell.detailTextLabel.text = nil;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            imageName = @"Icon-Statistics";
        }
        
        //    CHANGE PASSWORD
        else if (rowIndex == 3) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BacisCell"];
            cell.textLabel.text = NSLocalizedString(@"Change Password", nil);
            cell.detailTextLabel.text = nil;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            imageName = @"Icon-Password";
        }
        
        //    DELETE ACCOUNT
        else if (rowIndex == 4) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"BacisCell"];
            cell.textLabel.text = NSLocalizedString(@"Delete Account", nil);
            cell.detailTextLabel.text = nil;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            imageName = @"Icon-Delete";
        }
    }
    
    if (imageName) {
        cell.imageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:imageName];
    } else {
        cell.imageView.image = nil;
    }
    
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    UIUserInterfaceIdiom interface = [[UIDevice currentDevice] userInterfaceIdiom];
    
    self.lastSelectedIndexPath = indexPath;
    
//    NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    
    
    if (indexPath.section == 2) {
        NEVPNManager *manager = [NEVPNManager sharedManager];
        NSUInteger rowIndex = indexPath.row - (manager.connection.status == NEVPNStatusDisconnected || manager.connection.status == NEVPNStatusConnected);
        if (((manager.connection.status == NEVPNStatusDisconnected || manager.connection.status == NEVPNStatusConnected) ) && indexPath.row == 0) {
            [self performSegueWithIdentifier:NSStringFromClass([SNProtectionsController class]) sender:self];
        }
        ////    FREE QUOTAS
        else if (rowIndex == 0) {
            [self performSegueWithIdentifier:NSStringFromClass([SNFreeQuotasController class]) sender:self];
        }
        
        //    DEVICES
        else if (rowIndex == 1) {
            if (DEVICE_IS_IPAD) {
                [self performSegueWithIdentifier:NSStringFromClass([SNDevicesController class]) sender:self];
            } else {
                SNDevicesController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SNDevicesController"];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        
        // Statistic
        else if (rowIndex == 2) {
            [self performSegueWithIdentifier:NSStringFromClass([SNStatisticsController class]) sender:self];
        }
        
        //    CHANGE PASSWORD
        else if (rowIndex == 3) {
            [self performSegueWithIdentifier:NSStringFromClass([SNChangePasswordController class]) sender:self];
        }
        
        //    DELETE ACCOUNT
        else if (rowIndex == 4) {
            [self performSegueWithIdentifier:NSStringFromClass([SNDeleteAccountController class]) sender:self];
        }
    }
    
    
    // Secure Status Row
    if ([indexPath isEqual:[NSIndexPath indexPathForRow:0 inSection:0]] && DEVICE_IS_IPAD)
    {
        [self performSegueWithIdentifier:NSStringFromClass([SNSecureStatusController class]) sender:self];
    }
    else if (indexPath.section == 0) {
        NSInteger idx = 2 + ([[SNAppDelegate sharedDelegate] appearanceColor] == SNAppearanceColorRed);
        if (indexPath.row == idx) {
            [self performSegueWithIdentifier:NSStringFromClass([SNLocationsController class]) sender:self];
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IOSVersion() >= 8) {
        return 44.f;
    }
    else if (indexPath.section == 0 && indexPath.row == 0) {
        if ([self vnpStatus] == SNVPNStatusNone) {
            return DEVICE_IS_IPAD ? 70 : 180;
        } else {
            return 44;
        }
    }
//    else if (indexPath.section == 1) {
//        if (self.hasPlan) {
//            return 44;
//        } else {
//            return 0.1;
//        }
//    }
    
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? kTABLE_SECTION_HEIGHT : UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section != 0) {
        return nil;
    }
    
    // setup account with email header view
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), kTABLE_SECTION_HEIGHT)];
    NSString *imageName = @"Icon-User-Small";
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[[SNAppDelegate sharedDelegate] imageWithImageName:imageName]];
    [view addSubview:imageView];
    imageView.center = CGPointMake(CGRectGetMidX(imageView.bounds) + kROW_LEFT_MARGIN, CGRectGetMidY(view.frame));
    
    CGFloat fontSize = 15.f;
    NSDictionary *attr = @{ NSFontAttributeName : [UIFont myriadFontWithSize:fontSize], NSForegroundColorAttributeName : self.view.tintColor };
    NSString *str = [NSLocalizedString(@"Account", nil) stringByAppendingString:@": "];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:str attributes:attr];

    if (self.user.email) {
        NSDictionary *attrEmail = @{ NSFontAttributeName : [UIFont myriadFontWithSize:fontSize] , NSForegroundColorAttributeName : self.view.tintColor };
        NSAttributedString *strEmail = [[NSAttributedString alloc] initWithString:self.user.email attributes:attrEmail];
        [attrString appendAttributedString:strEmail];        
    }
    
    UILabel *label = [UILabel new];
    label.attributedText = attrString;
    label.lineBreakMode = NSLineBreakByTruncatingMiddle;
    CGSize size = CGSizeMake(CGRectGetWidth(self.view.frame) - CGRectGetMaxX(imageView.frame) - kROW_LEFT_MARGIN - 8, CGFLOAT_MAX);
    CGRect frame = [attrString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin context:NULL];
    frame.origin.x = CGRectGetMaxX(imageView.frame) + kROW_LEFT_MARGIN / 2;
    frame.origin.y = (CGRectGetHeight(view.frame) - CGRectGetHeight(frame)) / 2 + 2;
    label.backgroundColor = [UIColor clearColor];
    label.frame = frame;
    [view addSubview:label];
    
    return view;
    
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (section > 0) {
        UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
        header.textLabel.text = [header.textLabel.text capitalizedString];
        [self configureView];
        header.textLabel.textColor = self.view.tintColor;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    if (section > 0) {
        UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
        [self configureView];
        header.textLabel.textColor = self.view.tintColor;
        header.textLabel.textAlignment = NSTextAlignmentCenter;
    }
}


#pragma mark - Server Methods 
- (void)loadProfile {
    
    
    
    if ([[DMUser defaultUser] isValid]) {
        [SVProgressHUD show];
        NSURLRequest *request = [NSURLRequest requestWithAction:@"user.profile" params:nil requestType:NSURLRequestTypeGet];
        [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
            
//            NSLog(@"%s %@", __FUNCTION__, json);
            
            if (error) {
                [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
                
            } else {
                self.hasPlan = NO;
                [SVProgressHUD dismiss];
                
                self.user.adBlock = [[json valueForKeyPath:@"response.ad_block"] boolValue];
                
                if ([json valueForKeyPath:@"response.current_plan"]) {
                    self.hasPlan = YES;
                    self.planDictionary = [json valueForKeyPath:@"response.current_plan"];
                    
                } else {
                    self.hasPlan = NO;
                    self.planDictionary = nil;
                }
                
                self.user.protectionType = [[json valueForKeyPath:@"response.protection"] intValue];
                self.user.location = [DMLocation locationWithIdentifier:[json valueForKeyPath:@"response.server_location"]];
                self.user.adBlock = [[json valueForKeyPath:@"response.ad_block"] boolValue];
                
                [self.user save];
                
                
                
                if ( DEVICE_IS_IPAD ) {
                    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
                    if (!indexPath) {
                        if ([self.tableView numberOfSections] > 0) {
                            if ([self.tableView numberOfRowsInSection:0] > 0) {
                                indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                                [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                                [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
                            }
                        }
                    }
                }
                
            }
        }];
        
    } else {
        self.hasPlan = NO;
    }
    

}

#pragma mark - SNVPNManager Delegate
- (void)VPNManager:(SNVPNManager *)manager savedConfigurationWithError:(NSError *)error
{
    if (!error) {
        [self.tableView reloadData];
    }
}

- (void)VPNManager:(SNVPNManager *)manager startedConnectionWithError:(NSError *)error {
    if (!error) {
        // notification center observer works in this place
    } else {
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }
}



@end
