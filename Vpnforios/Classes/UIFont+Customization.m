//
//  UIFont+Customization.m
//  Vpnforios
//
//  Created by Шурик on 11.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "UIFont+Customization.h"

@implementation UIFont (Customization)

+ (UIFont *)myriadFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"MyriadPro-Regular" size:size];
}

+ (UIFont *)myriadBoldFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"MyriadPro-Bold" size:size];
}

+ (void)customizeLabel:(UILabel *)label
{
    label.font = [UIFont myriadFontWithSize:label.font.pointSize];
}

+ (void)customizeButton:(UIButton *)button
{
    button.titleLabel.font = [UIFont myriadFontWithSize:button.titleLabel.font.pointSize];
}

@end
