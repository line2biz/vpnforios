//
//  SNNotProtectedCell.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNNotProtectedCell.h"

#import "SNInstallController.h"

@interface SNNotProtectedCell ()

@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation SNNotProtectedCell


- (void)awakeFromNib
{
    self.button.titleLabel.numberOfLines = 2;
    self.button.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (IBAction)didTapButtonProtect:(id)sender {
}

@end
