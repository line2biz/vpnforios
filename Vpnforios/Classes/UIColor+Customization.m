//
//  UIColor+Customization.m
//  Vpnforios
//
//  Created by Шурик on 08.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "UIColor+Customization.h"

@implementation UIColor (Customization)

+ (UIColor *)mainBackgroundColor
{
    return [UIColor colorWithRed:239/255.f green:239/255.f blue:244/255.f alpha:1];
}

+ (UIColor *)customDarkGrayColor
{
    return [UIColor colorWithRed:109/255.f green:109/255.f blue:114/255.f alpha:1];
}

@end
