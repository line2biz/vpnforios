//
//  SNDeviceCell.h
//  Vpnforios
//
//  Created by Admin on 11/27/14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNDeviceCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *deviceImage;
@property (strong, nonatomic) IBOutlet UILabel *deviceLabel;

@end
