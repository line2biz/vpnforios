//
//  SNLoginController.m
//  Vpnforios
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNLoginController.h"

#import "SNHeaderView.h"
#import "UIColor+Customization.h"
#import "NSURLRequest+Customization.h"
#import "SVProgressHud.h"
#import "SNConnectionManager.h"
#import "DMUser.h"
#import "SNLoginCell.h"

@interface SNLoginController ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginButton;

@property (strong,nonatomic) IBOutlet UIImageView *emailImageView;
@property (strong,nonatomic) IBOutlet UIImageView *passwordImageView;
@property (strong,nonatomic) IBOutlet UIImageView *forgotImageView;
@property (strong,nonatomic) IBOutlet UIImageView *registerImageView;

@end

@implementation SNLoginController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.loginButton.enabled = NO;
    self.clearsSelectionOnViewWillAppear = YES;
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    
    UITableViewCell *red = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    red.accessoryType = UITableViewCellAccessoryNone;
    
    UITableViewCell *blue = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
    blue.accessoryType = UITableViewCellAccessoryNone;
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[[NSUserDefaults standardUserDefaults] integerForKey:SNAppTintColorKey] inSection:2]];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    self.uid = nil;
    self.access_token = nil;
    
    [self configureView];
    
#if DEBUG
    self.passwordTextField.text = @"test123";
    self.emailTextField.text = @"test123@test.com";
#endif
}

#pragma mark - Rotations

#pragma mark - Private Methods
- (void)configureView
{
    self.emailImageView.image    = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Email"];
    self.passwordImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Password"];
    self.forgotImageView.image   = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Forget"];
    self.registerImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-User"];
    
    self.emailTextField.placeholder = NSLocalizedString(@"Your email", nil);

}
- (BOOL)validateFieldsWithAlerts:(BOOL)showAlerts
{
    NSString *message = nil;
    NSInteger tag = NSNotFound;
    
    if (self.emailTextField.text.length >= 6) {
        if (self.passwordTextField.text.length >= 3) {
            if (![DMUser validateEmail:self.emailTextField.text]){
                message = NSLocalizedString(@"email not valid", nil);
                tag = self.emailTextField.tag;
            } else
                return YES;
        } else {
            message = NSLocalizedString(@"password must have much characters", nil);
            tag = self.passwordTextField.tag;
        }
    } else {
        message = NSLocalizedString(@"email must have much characters", nil);
        tag = self.emailTextField.tag;
    }
    
    if (showAlerts && message) {
        NSString *title = NSLocalizedString(@"Authorization", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alertView.tag = tag;//self.emailTextField.tag;   //??
        
        [alertView show];
    }
    
    return NO;
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    UITextField *textField = self.emailTextField;
    [textField becomeFirstResponder];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

- (IBAction)editingChanged:(UITextField *)sender
{
//    self.loginButton.enabled = NO;
//    
//    NSString *email    = self.emailTextField.text;
//    NSString *password = self.passwordTextField.text;
//    
//    if (email.length == 0 || password.length == 0)
//        return;
//    
//    if (![DMUser validateEmail:email])
//        return;
//    
//    self.loginButton.enabled = YES;
}

- (IBAction)didTapButtonLogin:(id)sender {
    [self login];
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    // Set the text color of our header/footer text.
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.tintColor;
    header.textLabel.text = [header.textLabel.text capitalizedString];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? SNTableSectionHeight : UITableViewAutomaticDimension;
}


- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.tintColor;
    header.textLabel.text = [header.textLabel.text capitalizedString];
    header.textLabel.textAlignment = NSTextAlignmentCenter;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"Selected section>> %d",indexPath.section);
//    NSLog(@"Selected row of section >> %d",indexPath.row);
    
    if (indexPath.section == 2) {
        NSInteger row = indexPath.row;
        
        [SNAppDelegate sharedDelegate].appearanceColor = (row == 0
                                                          ? SNAppearanceColorRed
                                                          : SNAppearanceColorBlue);
        
        [[NSUserDefaults standardUserDefaults] setInteger:row forKey:SNAppTintColorKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // removing marks
        UITableViewCell *redVersionCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        redVersionCell.accessoryType = UITableViewCellAccessoryNone;
        
        UITableViewCell *blueVersionCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
        blueVersionCell.accessoryType = UITableViewCellAccessoryNone;
        
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        [self.tableView reloadData];
        [self configureView];
    }
    
}

#pragma mark - Text Fields Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else {
        [self login];
    }
    
    return YES;
}

#pragma mark - Server Methods
- (void)login {
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:SNAppDeviceTokenKey];
//    if ([self validateFieldsWithAlerts:YES]) {
        NSDictionary *dictionary = @{
                                     @"password" : self.passwordTextField.text,
                                     @"email" : self.emailTextField.text,
                                     @"device" : deviceToken,
                                     @"device_name" : [[UIDevice currentDevice] name]
                                     };
        NSURLRequest *request = [NSURLRequest requestWithAction:@"user.login" params:dictionary  requestType:NSURLRequestTypePost];
        
        [SVProgressHUD show];
        
        [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
            
            [SVProgressHUD dismiss];
            
            if (error) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
                [alertView show];
            } else {
                NSString *map = [json valueForKey:@"response"];
                
                DMUser *user = [DMUser defaultUser];
                
                user.uid = [[map valueForKey:@"uid"] integerValue];
                user.accessToken = [map valueForKey:@"access_token"];
                
                user.password = self.passwordTextField.text;
                user.email = self.emailTextField.text;
                
                [user save];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:SNAppUserDidLogin object:nil];
            
                
                [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
            }
        }];
//    }
    
}

@end
