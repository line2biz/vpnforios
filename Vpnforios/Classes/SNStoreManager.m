//
//  SNStoreManager.m
//  Vpnforios
//
//  Created by Шурик on 30.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNStoreManager.h"

@import StoreKit;

@interface SNStoreManager () <SKProductsRequestDelegate /*, SKPaymentTransactionObserver */>
@property (strong, nonatomic) SKProductsRequest *productsRequest;
@end

@implementation SNStoreManager

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSSet *productIdentifiers = [self purchasedItems];
        _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
        _productsRequest.delegate = self;
        [_productsRequest start];
    }
    return self;
}

+ (SNStoreManager *)sharedManager {
    static SNStoreManager *__sharedManaer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedManaer = [[SNStoreManager alloc] init];
    });
    
    return __sharedManaer;
}

- (NSSet *)purchasedItems {
    NSMutableSet *set = [NSMutableSet new];
    [set addObject:@"com.netzsuche.vpn.1gb"];
    [set addObject:@"com.netzsuche.vpn.5gb"];
    [set addObject:@"com.netzsuche.vpn.10gb"];
    
    return set;
}

#pragma mark - SKProductsRequestDelegate methods

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *products = response.products;
   
    for (SKProduct *product in products) {
        NSLog(@"%s %@", __FUNCTION__, product);
    }
    
    for (NSString *invalidProductId in response.invalidProductIdentifiers) {
        NSLog(@"Invalid product id: %@" , invalidProductId);
    }
    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Failed to load list of products."
                                                      message:[NSString stringWithFormat:@"%@",error.localizedDescription]
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
}


@end
