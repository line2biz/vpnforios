//
//  DMLocation.h
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMLocation : NSObject

@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic, readonly) UIImage *image;

+ (NSArray *)locationsArray;
+ (DMLocation *)locationWithIdentifier:(NSString *)identifier;

@end
