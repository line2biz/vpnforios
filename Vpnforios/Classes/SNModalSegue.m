//
//  SNModalSegue.m
//  Vpnforios
//
//  Created by Andrey Serebryakov on 12.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNModalSegue.h"

@implementation SNModalSegue

- (void)perform {
    if (DEVICE_IS_IPHONE) {
        [[self sourceViewController] presentViewController:self.destinationViewController animated:YES completion:NULL];
    } else {
        UISplitViewController *splitViewController = (UISplitViewController *)((UINavigationController *)self.sourceViewController).parentViewController.parentViewController;
        
        if ([self.destinationViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *destinationNavigationController = (UINavigationController *)self.destinationViewController;
            UIViewController *destinationView = (UIViewController *)destinationNavigationController.childViewControllers[0];
            
            splitViewController.viewControllers = @[splitViewController.viewControllers[0], destinationNavigationController];
            [splitViewController performSelector:@selector(setDelegate:) withObject:destinationView];

        } else {
            UINavigationController *destinationNavigationController = (UINavigationController *)self.destinationViewController;
            
            splitViewController.viewControllers = @[splitViewController.viewControllers[0], destinationNavigationController];
            [splitViewController performSelector:@selector(setDelegate:) withObject:destinationNavigationController];
        }
        
    }
}

@end
