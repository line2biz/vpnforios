//
//  SNDataPlanController.m
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDataPlanController.h"

#import "DMDataPlan.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHeaderView.h"
#import "SNTransactionManager.h"


@import StoreKit;

@interface SNDataPlanController ()

@property (weak, nonatomic) IBOutlet UILabel *acciuntLabel;
@property (weak, nonatomic) IBOutlet UILabel *specificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandwichLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandWichDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *periodLabel;
@property (weak, nonatomic) IBOutlet UILabel *periodDetailLabel;

@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *quotaImageView;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@end

@implementation SNDataPlanController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (DEVICE_IS_IPHONE) {
        self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
        self.navigationItem.title = NSLocalizedString(@"Details", nil);
    } else {
        self.navigationItem.title = NSLocalizedString(@"Data Plans", nil);
    }
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.clearsSelectionOnViewWillAppear = YES;
    
    for (UILabel *label in self.labels) {
        label.font = [UIFont myriadFontWithSize:label.font.pointSize];
    }
    
    
    
    [self configureView];
    
    DMUser *user = [DMUser defaultUser];
    self.acciuntLabel.text = [@"Account: " stringByAppendingString:user.email];
    self.specificationLabel.text = self.product.localizedDescription;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:self.product.priceLocale];
    
    self.priceLabel.text = [numberFormatter stringFromNumber:self.product.price];
    self.priceLabel.textColor = self.view.tintColor;
    
    CGRect frame = self.priceLabel.frame;
    frame.size.width = 200;
    self.priceLabel.frame = frame;
    
    [self.priceLabel sizeToFit];
    
    
    self.brandwichLabel.text =  NSLocalizedString(@"Bandwidth", nil);
    self.brandWichDetailLabel.text = @"10 GB";
    self.periodLabel.text = NSLocalizedString(@"Validity Period", nil);
    
    NSString *identifier = self.product.productIdentifier;
    if ([[identifier substringFromIndex:(identifier.length - 4)] isEqualToString:@"days"]) {
        NSArray *array = [identifier componentsSeparatedByString:@"."];
        NSString *str = [[array lastObject] stringByReplacingOccurrencesOfString:@"days" withString:@""];
        
        NSInteger days = [str integerValue];
        self.periodDetailLabel.text = [NSString stringWithFormat:@"%zd days", days];
        
        
        NSString *str1 = @"";
        switch (days) {
            case 30:
                str1 = @"4 GB";
                break;
            case 60:
                str1 = @"10 GB";
                break;
                
            case 90:
                str1 = @"30 GB";
                break;
                
            case 120:
                str1 = @"80 GB";
                break;

            case 180:
                str1 = @"120 GB";
                break;

            case 365:
                str1 = @"300 GB";
                break;

            default:
                str1 = @"? GB";
                break;
        }
        self.brandWichDetailLabel.text = str1;
        
    } else {
        
        NSArray *array = [identifier componentsSeparatedByString:@"."];
        self.brandWichDetailLabel.text = [[array lastObject] stringByReplacingOccurrencesOfString:@"gb" withString:@" GB"];
        self.periodDetailLabel.text = NSLocalizedString(@"never expire", nil);
    }
    
    
    
    
    
}

#pragma mark - Private Methods
- (void)configureView
{
    self.view.tintColor = [[SNAppDelegate sharedDelegate] tintColor];
    self.emailImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Email"];
//    self.quotaImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Quota"];

}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonBuy:(id)sender {
    [[SNTransactionManager sharedManager] buyProduct:self.product];
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text = [header.textLabel.text capitalizedString];
    header.textLabel.textColor = self.view.tintColor;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? SNTableSectionHeight : UITableViewAutomaticDimension;
}





@end
