//
//  SNAppDelegate.h
//  Vpnforios
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "SNAppearance.h"

#import "DMUser.h"
#import "DMLocation.h"

// Notification
FOUNDATION_EXTERN NSString * const SNAppUserDidLogin;
FOUNDATION_EXTERN NSString * const SNAppUserDidDeleted;
FOUNDATION_EXTERN NSString * const SNAppMessageRead;
FOUNDATION_EXTERN NSString * const SNAppDeviceTokenKey;

FOUNDATION_EXTERN NSString * const SNAppTintColorKey;

FOUNDATION_EXTERN CGFloat const SNTableSectionHeight;

@protocol SNAppDelegateForegroundProtocol;
@protocol SNAppDelegateStartupProtocol;

@interface SNAppDelegate : UIResponder <UIApplicationDelegate, UISplitViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UISplitViewController *splitViewController;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (assign, nonatomic) NSUInteger badgeNumber;

- (UIColor *)tintColor;
+ (SNAppDelegate *)sharedDelegate;
- (UIImage *)imageWithImageName:(NSString *)imageName;

@property (assign, nonatomic) SNAppearanceColor appearanceColor;

/// Validation Methods
+ (BOOL)isPasswordValid:(NSString *)pwd error:(NSError **)error;
+ (BOOL)isEmailValid:(NSString *)emil error:(NSError **)error;

@end


@protocol SNAppDelegateForegroundProtocol <NSObject>
@required
- (void)applicationWillAppearInForeground;

@end

@protocol SNAppDelegateStartupProtocol <NSObject>
@required
- (void)applicationDidStart;

@end

float IOSVersion();