//
//  SNDevicesController.m
//  Vpnforios
//
//  Created by Admin on 11/27/14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDevicesController.h"

#import "SNConnectionManager.h"
#import "NSURLRequest+Customization.h"
#import "UIColor+Customization.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "SNDeviceCell.h"
#import "SNDeviceDetailController.h"
#import "SNHeaderView.h"

@import ObjectiveC.runtime;

@interface SNDevicesController ()

@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) NSDictionary *device;

#define kDEVICE_TOKEN   @"device"
#define kDEVICE_ID      @"id"
#define kDEVICE_NAME    @"name"

typedef void (^AlertViewBlock)(UIAlertView *alertView, NSInteger buttonIndex);
#define kALERT_VIEW_BLOCK  @"kALERT_VIEW_BLOCK"

@end

@implementation SNDevicesController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = NSLocalizedString(@"My Devices", nil);
    
    self.device = @{
                    kDEVICE_TOKEN : [[NSUserDefaults standardUserDefaults] valueForKey:SNAppDeviceTokenKey],
                    kDEVICE_NAME : [[UIDevice currentDevice] name]
                    };
    
    if (DEVICE_IS_IPHONE) {
        self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    }
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    [self loadDevicesFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Private Methods

- (void) parseDevicesFromDictionary:(NSDictionary *)dict {
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:5];
    for (NSDictionary *dictionary in dict[@"response"]) {
        if ( ![self.device[kDEVICE_TOKEN] isEqualToString:dictionary[kDEVICE_TOKEN]]) {
            [array addObject:dictionary];
        }
    }
    
    self.items = array;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 + (self.items.count > 0);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return NSLocalizedString(@"Current device", nil);
    } else {
        return NSLocalizedString(@"Other devices", nil);
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return [self.items count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"SNDeviceCell";
    SNDeviceCell *cell = (SNDeviceCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.section == 0) {
        cell.deviceLabel.text = self.device[kDEVICE_NAME];
    }else {
       cell.deviceLabel.text = [self.items[indexPath.row] valueForKey:@"name"];
    }
    cell.deviceImage.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Devices"];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        __weak typeof(self) weakSelf = self;
        AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (buttonIndex == alertView.cancelButtonIndex){
                strongSelf.tableView.editing = NO;
            }else {
                [strongSelf deleteDeviceForRowAtIndexPath:(NSIndexPath *)indexPath];
            }
        };
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Delete Device?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"No", nil) otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
        objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
        [alertView show];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    SNDeviceDetailController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SNDeviceDetailController"];
//    controller.device = _devices[indexPath.row];
//    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text = [header.textLabel.text capitalizedString];
    header.textLabel.textColor = self.view.tintColor;
}


#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    AlertViewBlock block = objc_getAssociatedObject(alertView, kALERT_VIEW_BLOCK);
    if (block) {
        block(alertView, buttonIndex);
    }
}

#pragma mark - Server Methods

- (void)loadDevicesFromServer {
    [SVProgressHUD show];
    __weak typeof(self) weakSelf = self;
    DMUser *user = [DMUser defaultUser];
    if (user.uid) {
        NSDictionary *params = @{ @"user_id" : @(user.uid),
                                  @"access_token" : user.accessToken
                                  };
        [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.devices" params:params requestType:NSURLRequestTypeGet] completionHandler:^(NSDictionary *json, NSError *error) {
            typeof(weakSelf) strongSelf = weakSelf;
            
            if (error) {
                [SVProgressHUD dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            } else {
                [strongSelf parseDevicesFromDictionary:json];
                [strongSelf.tableView reloadData];
                [SVProgressHUD dismiss];
            }
        }];
    }
}


- (void)deleteDeviceForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *deviceId = [self.items[indexPath.row] objectForKey:@"id"];
    [SVProgressHUD show];
    __weak typeof(self) weakSelf = self;
    DMUser *user = [DMUser defaultUser];
    if (user.uid) {
        NSDictionary *params = @{ @"device_id" : deviceId, @"access_token" : user.accessToken };
        [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.deleteDevice" params:params requestType:NSURLRequestTypeGet] completionHandler:^(NSDictionary *json, NSError *error) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (error) {
                [SVProgressHUD dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            } else {
                [SVProgressHUD dismiss];
                [strongSelf.items removeObjectAtIndex:indexPath.row];
                [strongSelf.tableView reloadData];
            }
        }];
    }
}
@end
