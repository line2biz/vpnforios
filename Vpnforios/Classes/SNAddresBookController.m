//
//  SNAddresBookController.m
//  Wagawin
//
//  Created by Шурик on 18.09.14.
//  Copyright (c) 2014 Wagawin. All rights reserved.
//

#import "SNAddresBookController.h"

#import "SNAddressBookCell.h"
#import "NSURLRequest+Customization.h"
#import "SNConnectionManager.h"

#import <SVProgressHUD/SVProgressHUD.h>

@import AddressBook;
@import MessageUI;
@import ObjectiveC.runtime;

#define kTABLE_CELL_IDENTIFIER  @"Cell"

#define kPROPERTY_FIRST_NAME    @"kPROPERTY_FIRST_NAME"
#define kPROPERTY_LAST_NAME     @"kPROPERTY_LAST_NAME"
#define kPROPERTY_PHONE         @"kPROPERY_PHONE"
#define kPROPERTY_EMAIL         @"kPROPERY_EMAIL"
#define kPROPERTY_COMPANY       @"kPROPERTY_COMPANY"

typedef void (^AlertViewBlock)(UIAlertView *alertView, NSInteger buttonIndex);
#define kALERT_VIEW_BLOCK  @"kALERT_VIEW_BLOCK"

@interface SNAddresBookController () <MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) MFMailComposeViewController *mailComposer;

@end

@implementation SNAddresBookController

#pragma mark - Initialization
- (instancetype)initWithSendType:(SNAddressBookSendType)sendType {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        _sendType = sendType;
        self.tableView.allowsMultipleSelectionDuringEditing = YES;
    }
    return self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.editing = YES;
    
    
//    Navigation Bar Items
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didTapButtonCancel:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Invite", nil) style:UIBarButtonItemStylePlain target:self action:@selector(didTapButtonInvite:)];
    self.navigationItem.rightBarButtonItem = sendButton;

//    Table Cell Class
    [self.tableView registerClass:[SNAddressBookCell class] forCellReuseIdentifier:kTABLE_CELL_IDENTIFIER];
    

//    Address book
    if (self.sendType == SNAddressBookSendTypeSMS || self.sendType == SNAddressBookSendTypeEmail) {
        //    Address Access
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied || ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
            NSLog(@"Denied");
            ///TODO: Is it neccessary to show information how to grant permissions
            
        } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
            [self loadPersonsFromAddressBook];
        } else {
            ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
                if (granted){
                    [self loadPersonsFromAddressBook];
                }
            });
        }
        
        if (self.sendType == SNAddressBookSendTypeEmail) {
            [self cycleMailComposer];
        }
        
    }
    else if (self.sendType == SNAddressBookSendTypeFacebook) {
//        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
//            //        [FBSession.activeSession closeAndClearTokenInformation];
//            [self loadFacebookFriends];
//            
//        } else {
//            // Open a session showing the user the login UI
//            // You must ALWAYS ask for public_profile permissions when opening a session
//            
//            NSString *title = @"Facebook";
//            NSString *msg = NSLocalizedString(@"You are not authorized.", nil);
//            NSString *cancelMsg = NSLocalizedString(@"Cancel", nil);
//            NSString *yesMsg = NSLocalizedString(@"Authorize", nil);
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:cancelMsg otherButtonTitles:yesMsg, nil];
//            [alertView show];
//            
//        }
    }
    
    
    
    [self configureView];
}

#pragma mark - Private Methods
- (void)configureView {
    self.navigationItem.rightBarButtonItem.enabled = [[self.tableView indexPathsForSelectedRows] count];
}

- (void)cycleMailComposer
{
    self.mailComposer = nil;
    self.mailComposer = [[MFMailComposeViewController alloc] init];
}



#pragma mark - Outlet Methods
- (IBAction)didTapButtonCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didTapButtonInvite:(id)sender {
    NSMutableArray *contacts = [NSMutableArray new];
    
    NSArray *array = [self.tableView indexPathsForSelectedRows];
    for (NSIndexPath *indexPath in array) {
        [contacts addObject:self.items[indexPath.row][kPROPERTY_EMAIL]];
    }
    
//    action = affiliate.invite
//    user_id
//    emails - массив имейлов
//    access_token
    
    __weak typeof(self) weakSelf = self;
    
    NSDictionary *pararams = @{ @"emails" : [contacts componentsJoinedByString:@","] };
    NSURLRequest *request = [NSURLRequest requestWithAction:@"affiliate.invite" params:pararams requestType:NSURLRequestTypePost];
    [SVProgressHUD show];
    [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
        [SVProgressHUD dismiss];
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            if (error) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Dismiss", nil), nil];
                [alertView show];
            } else {
                __weak typeof(self) weakSelf = self;
                AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
                    [weakSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                };
                NSString *title = NSLocalizedString(@"Invitation", nil);
                NSString *msg = NSLocalizedString(@"Invitation was sent.", nil);
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
                [alertView show];
                
            }
        }
    }];
    
    
    return;
    
    if (self.sendType == SNAddressBookSendTypeSMS) {
        NSArray *recipients = [contacts valueForKeyPath:kPROPERTY_PHONE];
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if ([MFMessageComposeViewController canSendText]) {
            controller.body = self.messageBody;
            controller.recipients = recipients;
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
    else if (self.sendType == SNAddressBookSendTypeEmail) {
        NSArray *recipients = [contacts valueForKeyPath:kPROPERTY_EMAIL];
        if ([MFMailComposeViewController canSendMail]) {
            [self.mailComposer setSubject:self.messageSubject];
            [self.mailComposer setMessageBody:self.messageBody isHTML:NO];
            [self.mailComposer setToRecipients:recipients];
            self.mailComposer.mailComposeDelegate = self;
            
            [self presentViewController:self.mailComposer animated:YES completion:nil];
        } else {
            [self cycleMailComposer];
        }
    }
    else if (self.sendType == SNAddressBookSendTypeFacebook) {
        
//        NSDictionary<FBGraphUser>* friend =   [contacts firstObject];
//        
//        NSString *facebookID = friend.objectID;
//        NSMutableDictionary* params =
//        [NSMutableDictionary dictionaryWithObject:facebookID forKey:@"to"];
//        
//        
//        [FBWebDialogs presentRequestsDialogModallyWithSession:nil
//                                                      message:self.messageBody
//                                                        title:self.messageSubject
//                                                   parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//                                                       if (error)
//                                                       {
//                                                           // Case A: Error launching the dialog or sending request.
//                                                           NSLog(@"Error sending request.");
//                                                       }
//                                                       else
//                                                       {
//                                                           if (result == FBWebDialogResultDialogNotCompleted)
//                                                           {
//                                                               // Case B: User clicked the "x" icon
//                                                               NSLog(@"User canceled request.");
//                                                           }
//                                                           else
//                                                           {
//                                                               NSLog(@"Request Sent. %@", params);
//                                                           }
//                                                       }}];
    }
}

#pragma mark - Message Composer Controller Delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    // Notifies users about errors associated with the interface
    NSString *resultString = nil;
    switch (result) {
        case MessageComposeResultCancelled:
            resultString = @"Result: SMS sending canceled";
            break;
        case MessageComposeResultSent:
            resultString = @"Result: SMS sent";
            break;
        case MessageComposeResultFailed:
            resultString = @"Result: SMS sending failed";
            break;
        default:
            resultString = @"Result: SMS not sent";
            break;
    }
    
    NSLog(@"%s %@", __FUNCTION__, resultString);
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Mail Compose View Controller Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    [self cycleMailComposer];
}

#pragma mark - Table View Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTABLE_CELL_IDENTIFIER forIndexPath:indexPath];
    
    
    if (self.sendType == SNAddressBookSendTypeFacebook) {
//        NSDictionary<FBGraphUser>* friend = self.items[indexPath.row];
//        cell.textLabel.text = friend.name;
        
    } else {
        NSDictionary *person = self.items[indexPath.row];
        
        NSString *textForLabel = @"";
        
        
        if ([person[kPROPERTY_FIRST_NAME] length] > 0 || [person[kPROPERTY_LAST_NAME] length] > 0) {
            textForLabel = [NSString stringWithFormat:@"%@ %@", person[kPROPERTY_FIRST_NAME], person[kPROPERTY_LAST_NAME]];
            
        } else {
            textForLabel = [NSString stringWithFormat:@"%@", person[kPROPERTY_COMPANY]];
        }
        
        cell.textLabel.text = textForLabel;
        
        NSString *textForDetailLabel = @"";
        switch (self.sendType) {
            case SNAddressBookSendTypeSMS:
                textForDetailLabel = person[kPROPERTY_PHONE];
                break;
                
            case SNAddressBookSendTypeEmail:
                textForDetailLabel = person[kPROPERTY_EMAIL];
                break;
                
            default:
                break;
        }
        cell.detailTextLabel.text = textForDetailLabel;
    }
    
    return cell;
    
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self configureView];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self configureView];
}

#pragma mark - Facebook API Methods
- (void)loadFacebookFriends {

//    __weak typeof(self) weakSelf = self;
//    
//    self.progressHUD = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleLight];
//    self.progressHUD.textLabel.text = @"loading..";
//    [self.progressHUD showInView:self.view];
//    
//    [FBSession openActiveSessionWithReadPermissions:@[@"user_friends"] allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//        FBRequest *friendsRequest = [FBRequest requestForGraphPath:@"me/taggable_friends"];
//        [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection, NSDictionary* result, NSError *error) {
//                weakSelf.items = [result objectForKey:@"data"];
//                [weakSelf.progressHUD dismissAnimated:YES];
//                [weakSelf.tableView reloadData];
//        }];
//        
//    }];
}

#pragma mark - Address Book Methods
- (void)loadPersonsFromAddressBook
{
    __weak typeof(self) weakSelf = self;
    
    
    [SVProgressHUD show];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, nil);
        CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
        CFMutableArrayRef peopleCFMutable = CFArrayCreateMutableCopy(kCFAllocatorDefault, CFArrayGetCount(people), people);
        CFIndex countPeople = CFArrayGetCount(people);
        
        // sorting contacts
        CFArraySortValues(peopleCFMutable,CFRangeMake(0, countPeople), (CFComparatorFunction) ABPersonComparePeopleByName, (void *)(NSUInteger) ABPersonGetSortOrdering());
        
        NSMutableArray *peopleNSMutable = [NSMutableArray new];
        
        for (int i = 0; i < countPeople; ++i) {
            ABRecordRef person = CFArrayGetValueAtIndex(peopleCFMutable, i);
            
            NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
            firstName = firstName ? firstName : @"";
            
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
            lastName = lastName ? lastName : @"";
            
            NSString *company = CFBridgingRelease(ABRecordCopyValue(person, kABPersonOrganizationProperty));
            company = (company ? company : @"");
            
            //            NSString *fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            
            if (self.sendType == SNAddressBookSendTypeSMS) {
                ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
                CFArrayRef phonesCFArrayRef = ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
                
                if (phonesCFArrayRef) {
                    if (CFArrayGetCount(phonesCFArrayRef) > 0) {
                        NSArray *phonesNSArray = (__bridge NSArray*) phonesCFArrayRef;
                        for (NSString *phone in phonesNSArray) {
                            NSDictionary *dictionary = @{ kPROPERTY_FIRST_NAME : firstName,
                                                          kPROPERTY_LAST_NAME  : lastName,
                                                          kPROPERTY_COMPANY    : company,
                                                          kPROPERTY_PHONE      : phone };
                            [peopleNSMutable addObject:dictionary];
                        }
                        
                    }
                    
                    CFRelease(phonesCFArrayRef);
                }
                
                CFRelease(phoneNumberProperty);
            }
            else if (self.sendType == SNAddressBookSendTypeEmail) {
                ABMultiValueRef emailABMultiValueRef = ABRecordCopyValue(person, kABPersonEmailProperty);
                CFArrayRef emailsCFArrayRef = ABMultiValueCopyArrayOfAllValues(emailABMultiValueRef);
                
                if (emailsCFArrayRef) {
                    if (CFArrayGetCount(emailsCFArrayRef) > 0) {
                        NSArray *phonesNSArray = (__bridge NSArray*)emailsCFArrayRef;
                        for (NSString *email in phonesNSArray) {
                            NSDictionary *dictionary = @{ kPROPERTY_FIRST_NAME : firstName,
                                                          kPROPERTY_LAST_NAME  : lastName,
                                                          kPROPERTY_COMPANY    : company,
                                                          kPROPERTY_EMAIL      : email };
                            [peopleNSMutable addObject:dictionary];
                        }
                        
                    }
                    
                    CFRelease(emailsCFArrayRef);
                }
                
                CFRelease(emailABMultiValueRef);
                
            }
            
            
            
        }
        
        self.items = peopleNSMutable;
        
        CFRelease(people);
        CFRelease(peopleCFMutable);
        CFRelease(addressBook);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [weakSelf.tableView reloadData];
        });
        
    });
    
    
    
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    AlertViewBlock block = objc_getAssociatedObject(alertView, kALERT_VIEW_BLOCK);
    if (block) {
        block(alertView, buttonIndex);
    }
}



@end
