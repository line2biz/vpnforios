//
//  DMMessage.h
//  Vpnforios
//
//  Created by Andrey Serebryakov on 08.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DMMessage : NSManagedObject

@property (nonatomic, retain) NSString * body;
@property (nonatomic) NSTimeInterval createdAt;
@property (nonatomic) int64_t identifier;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic) BOOL isUnread;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * brief;

@end
