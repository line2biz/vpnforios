//
//  SNTransactionManager.h
//  Vpnforios
//
//  Created by Шурик on 06.10.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString * const SNTransactionManagerDidReceivePayment;

@class SKProduct;

@interface SNTransactionManager : NSObject

+ (SNTransactionManager *)sharedManager;
- (void)buyProduct:(SKProduct *)product;

@end
