//
//  DMQuestion+Category.h
//  Vpnforios
//
//  Created by Шурик on 10.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMQuestion.h"

@interface DMQuestion (Category)

+ (DMQuestion *)questionWithIdentifier:(NSNumber *)identifer inContext:(NSManagedObjectContext *)context;
+ (void)parseResponse:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context;

@end
