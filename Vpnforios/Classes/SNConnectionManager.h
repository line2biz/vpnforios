//
//  SNConnectionManager.h
//  First
//
//  Created by Andrey Serebryakov on 01.09.14.
//  Copyright (c) 2014 Andrey Serebryakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

FOUNDATION_EXTERN NSString * const SNConnectionManagerInvalidAccessTokenKey;

@interface SNConnectionManager : NSObject

+ (NSURLConnection *)sendRequest:(NSURLRequest *)request completionHandler:(void (^)(NSDictionary* json, NSError* error)) handler;

@end
