//
//  SNMessagesController.m
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNMessagesController.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHeaderView.h"
#import "DMMessage+Category.h"
#import "SNConnectionManager.h"
#import "NSURLRequest+Customization.h"
#import "DMManager.h"
#import "SNMessageController.h"
#import "SNModalSegue.h"

#import <SVProgressHUD/SVProgressHUD.h>

@interface SNMessagesController () <NSFetchedResultsControllerDelegate, SNAppDelegateStartupProtocol>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSMutableArray *items;

@property (assign, nonatomic) BOOL isFirstMessageShown;
@property (strong, nonatomic) NSIndexPath *lastIndexPath;

@end

@implementation SNMessagesController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.managedObjectContext = [[DMManager sharedManager] defaultContext];
    
    if (DEVICE_IS_IPHONE) {
        self.navigationItem.title = NSLocalizedString(@"Messages", nil);
    } else {
        self.navigationItem.title = nil;
    }
    
    
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.clearsSelectionOnViewWillAppear = YES;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self loadMessagesFromServer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didGetSNAppMessageRead) name:SNAppMessageRead object:nil];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadMessagesFromServer) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    self.isFirstMessageShown = false;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (DEVICE_IS_IPAD)
    {
        if ([self.tableView numberOfRowsInSection:0] == 0) {
            return;
        }
        
        if (!self.isFirstMessageShown) {
            self.isFirstMessageShown = true;
            
            self.lastIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView selectRowAtIndexPath:self.lastIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            [self performSegueWithIdentifier:NSStringFromClass([SNMessageController class]) sender:self];
        }
        else if (self.lastIndexPath)
        {
            [self.tableView selectRowAtIndexPath:self.lastIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didGetSNAppMessageRead {
    [self.tableView reloadData];
    [[SNAppDelegate sharedDelegate] setBadgeNumber:[DMMessage unreadMessagesCount]];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue destinationViewController] isKindOfClass:[UINavigationController class]]) {
//        UINavigationController *navController = (UINavigationController *)[segue destinationViewController];
//        SNFriendController *friendController = (SNFriendController *)[navController topViewController];
//        friendController.managedObjectContext = self.managedObjectContext;
    }
    else if ([[segue destinationViewController] isKindOfClass:[SNMessageController class]])
    {
        SNMessageController *messageController = [segue destinationViewController];
        messageController.managedObjectContext = self.managedObjectContext;
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DMMessage * message = [self.fetchedResultsController objectAtIndexPath:indexPath];
        messageController.message = message;
    }
}


#pragma mark - Outlet Methods
- (IBAction)didTapButtonRefresh:(id)sender {
    [self loadMessagesFromServer];
}


#pragma mark - App Delegate Protocol
- (void)applicationDidStart {
    [self loadMessagesFromServer];
}


#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    DMMessage *message = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = message.title;
    cell.detailTextLabel.text = message.brief;
    
    if (message.isUnread) {
        UIFont* boldFont = [UIFont boldSystemFontOfSize:cell.textLabel.font.pointSize];
        [cell.textLabel setFont:boldFont];
    } else {
        UIFont* boldFont = [UIFont systemFontOfSize:cell.textLabel.font.pointSize];
        [cell.textLabel setFont:boldFont];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.lastIndexPath = indexPath;
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMMessage class])];
    NSSortDescriptor *first = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO];
    request.sortDescriptors = @[first];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Server Methods

- (void)loadMessagesFromServer {
    __weak typeof(self) weakSelf = self;
    DMUser *user = [DMUser defaultUser];
    if (user.uid) {
        NSDictionary *params = @{ @"user_id" : @(user.uid),
                                  @"access_token" : user.accessToken
                                  };
        [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.news" params:params requestType:NSURLRequestTypeGet] completionHandler:^(NSDictionary *json, NSError *error) {
            typeof(weakSelf) strongSelf = weakSelf;
            
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            } else {
    //            NSLog(@"%@", json);
                [DMMessage parseResponseArray:[json valueForKeyPath:@"response.news"] inContext:[[DMManager sharedManager] defaultContext]];
                
                NSString *unreadCount = [json valueForKeyPath:@"response.unread"];
    //            self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%@", unreadCount];
                
                [[SNAppDelegate sharedDelegate] setBadgeNumber:[unreadCount integerValue]];
            }
            
            [[strongSelf refreshControl] endRefreshing];
       }];
    }
}

@end
