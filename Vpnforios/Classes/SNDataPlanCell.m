//
//  SNDataPlanCell.m
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDataPlanCell.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"

@implementation SNDataPlanCell


- (void)awakeFromNib
{
    self.titleLabel.font = [UIFont myriadBoldFontWithSize:self.titleLabel.font.pointSize];
    self.subtitleLabel.font = [UIFont myriadFontWithSize:self.subtitleLabel.font.pointSize];
    [self configureView];
}

- (void)configureView
{
    self.priceLabel.textColor = [[SNAppDelegate sharedDelegate] tintColor];
}

//- (void)layoutSubviews {
//    [self.textLabel sizeToFit];
//    
//    NSLog(@"Frame: %f", self.subtitleLabel.frame.size.height);
////    [self.subtitleLabel sizeThatFits:CGSizeMake(self.subtitleLabel.frame.size.width, MAXFLOAT)];
//    CGRect f = self.subtitleLabel.frame;
//    f.size.height = 17*2;
//    self.subtitleLabel.frame = f;
//    NSLog(@"Frame after: %f", self.subtitleLabel.frame.size.height);
//}

@end
