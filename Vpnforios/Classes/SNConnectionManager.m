//
//  SNConnectionManager.m
//  First
//
//  Created by Andrey Serebryakov on 01.09.14.
//  Copyright (c) 2014 Andrey Serebryakov. All rights reserved.
//

#import "SNConnectionManager.h"
#import <objc/runtime.h>

#define kRESPONSE_DATA @"kRESPONSE_DATA"
#define kCOMPLETION_HANDLER @"kCOMPLETITION_HANDLER"

NSString * const SNConnectionManagerInvalidAccessTokenKey = @"SNConnectionManagerInvalidAccessTokenKey";

typedef void (^CompletionHandler)(NSDictionary *, NSError *);

@interface SNConnectionManager ()

@property (strong, nonatomic) NSMutableSet *connections;

@end

@implementation SNConnectionManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        _connections = [NSMutableSet new];
    }
    return self;
}

- (void)startConnection:(NSURLConnection *)connection {
    [connection start];
    
    [self.connections addObject:connection];
    [self updateNetworkActivity];
}

- (void)stopConnection:(NSURLConnection *)connection {
    [connection cancel];
    [self.connections removeObject:connection];
    [self updateNetworkActivity];
    
}

- (void)updateNetworkActivity {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:[self.connections count]];
}

+ (SNConnectionManager *)defaultManager {
    static SNConnectionManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SNConnectionManager alloc] init];
    });
    return manager;
}

+ (NSURLConnection *)sendRequest:(NSURLRequest *)request completionHandler:(void (^)(NSDictionary *, NSError *))handler {
    return [[SNConnectionManager defaultManager] sendRequest:request completionHandler:handler];
}

- (NSURLConnection *)sendRequest:(NSURLRequest *)request completionHandler:(void (^)(NSDictionary *, NSError *))handler {
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    objc_setAssociatedObject(connection, kCOMPLETION_HANDLER, handler, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self startConnection:connection];
    return connection;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSMutableData *responseData = [NSMutableData new];
    objc_setAssociatedObject(connection, kRESPONSE_DATA, responseData, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSMutableData *responseData = objc_getAssociatedObject(connection, kRESPONSE_DATA);
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    CompletionHandler block = objc_getAssociatedObject(connection, kCOMPLETION_HANDLER);
    block(nil, error);
    [self stopConnection:connection];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    CompletionHandler block = objc_getAssociatedObject(connection, kCOMPLETION_HANDLER);
    NSMutableData *responseData = objc_getAssociatedObject(connection, kRESPONSE_DATA);
    NSError *error = nil;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments | NSJSONReadingMutableContainers  error:&error];
    if (error) {
        block(nil, error);
    } else {
        if (json[@"error"]) {
            
            NSLog(@"%s %@", __FUNCTION__, json);
            
            NSInteger code = [[json valueForKeyPath:@"error.code"] integerValue];
            NSString *description = [json valueForKeyPath:@"error.description"];
            error = [NSError errorWithDomain:@"vpnforios" code:code userInfo:[NSDictionary dictionaryWithObject:description forKey:NSLocalizedDescriptionKey]];
            
            if (code == 107) {
                [[NSNotificationCenter defaultCenter] postNotificationName:SNConnectionManagerInvalidAccessTokenKey object:error];
                [self stopConnection:connection];
                return;
            }
        }
        
        block(json, error);
    }
    
    [self stopConnection:connection];
}


@end
