//
//  DMMessage.m
//  Vpnforios
//
//  Created by Andrey Serebryakov on 08.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMMessage.h"


@implementation DMMessage

@dynamic body;
@dynamic createdAt;
@dynamic identifier;
@dynamic imageURL;
@dynamic isUnread;
@dynamic title;
@dynamic brief;

@end
