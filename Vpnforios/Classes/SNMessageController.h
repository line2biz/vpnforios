//
//  SNMessageController.h
//  Vpnforios
//
//  Created by Шурик on 09.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMMessage;

@interface SNMessageController : UITableViewController

@property (strong, nonatomic) DMMessage *message;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
