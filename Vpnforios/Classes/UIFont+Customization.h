//
//  UIFont+Customization.h
//  Vpnforios
//
//  Created by Шурик on 11.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Customization)

+ (UIFont *)myriadFontWithSize:(CGFloat)size;
+ (UIFont *)myriadBoldFontWithSize:(CGFloat)size;

+ (void)customizeLabel:(UILabel *)label;
+ (void)customizeButton:(UIButton *)button;
@end
