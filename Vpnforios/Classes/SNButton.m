//
//  SNButton.m
//  Vpnforios
//
//  Created by Шурик on 11.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNButton.h"

#import "SNButton.h"
#import "UIFont+Customization.h"

@implementation SNButton

- (void)awakeFromNib
{
    [UIFont customizeButton:self];
    [self configureView];
}

- (void)tintColorDidChange
{
    [self configureView];
}

- (void)configureView
{
    UIImage *image=nil;
    if ([[SNAppDelegate sharedDelegate]appearanceColor]==SNAppearanceColorRed) {
        image = [UIImage imageNamed:@"Btn-Bkg-h53-Red"];
    } else {
        image = [UIImage imageNamed:@"Btn-Bkg-h53-Blue"];
    }
    [self setBackgroundImage:image forState:UIControlStateNormal];
}

@end
