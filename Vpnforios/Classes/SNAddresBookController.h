//
//  SNAddresBookController.h
//  Wagawin
//
//  Created by Шурик on 18.09.14.
//  Copyright (c) 2014 Wagawin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SNAddressBookSendType) {
    SNAddressBookSendTypeSMS        = 0,
    SNAddressBookSendTypeEmail      = 1 << 0,
    SNAddressBookSendTypeFacebook   = 1 << 1
};

@interface SNAddresBookController : UITableViewController

@property (nonatomic) SNAddressBookSendType sendType;
- (instancetype)initWithSendType:(SNAddressBookSendType)sendType;

@property (strong, nonatomic) NSString *messageBody;
@property (strong, nonatomic) NSString *messageSubject;

@end
