//
//  SNDataPlanController.h
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SKProduct;

@interface SNDataPlanController : UITableViewController

@property (strong, nonatomic) SKProduct *product;

@end
