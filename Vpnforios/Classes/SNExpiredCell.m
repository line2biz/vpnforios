//
//  SNExpiredCell.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNExpiredCell.h"

#import "UIFont+Customization.h"
#import "UIColor+Customization.h"


@interface SNExpiredCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;


@end

@implementation SNExpiredCell


- (void)awakeFromNib
{
    [self configureView];
}

- (void)configureView
{
    self.titleLabel.textColor = [[SNAppDelegate sharedDelegate] tintColor];
    self.subtitleLabel.textColor = [UIColor customDarkGrayColor];
}



@end
