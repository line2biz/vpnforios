//
//  DMLocation.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMLocation.h"

NSString * const DMLocationIdentifierKey = @"identifier";

@implementation DMLocation

@dynamic image;

+ (NSArray *)locationsArray
{
    static NSArray *locationsArray = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableArray *array = [NSMutableArray new];
        
        DMLocation *location = [DMLocation new];
        location.identifier = @"CH";
        location.title = NSLocalizedString(@"Switzerland", nil);
        [array addObject:location];
        
        location = [DMLocation new];
        location.identifier = @"DE";
        location.title = NSLocalizedString(@"Germany", nil);
        [array addObject:location];
        
        location = [DMLocation new];
        location.identifier = @"UK";
        location.title = NSLocalizedString(@"United Kingdom", nil);
        [array addObject:location];

        location = [DMLocation new];
        location.identifier = @"US";
        location.title = NSLocalizedString(@"United States", nil);
        [array addObject:location];
        
        locationsArray = [NSArray arrayWithArray:array];
        
    });
    
    return locationsArray;
}

+ (DMLocation *)locationWithIdentifier:(NSString *)identifier
{
    DMLocation *location = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", DMLocationIdentifierKey, identifier];
    location = [[[DMLocation locationsArray] filteredArrayUsingPredicate:predicate] firstObject];
    if (!location) {
        location = [[DMLocation locationsArray] firstObject];
    }
    return location;
}


#pragma mark - Property Accessors
- (UIImage *)image
{
    NSString *imageName = [@"Image-Flag-" stringByAppendingString:self.identifier];
    return [UIImage imageNamed:imageName];
}

@end
