//
//  DMQuestion.m
//  Vpnforios
//
//  Created by Andrey Serebryakov on 10.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMQuestion.h"


@implementation DMQuestion

@dynamic identifier;
@dynamic title;
@dynamic body;
@dynamic date;

@end
