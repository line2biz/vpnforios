//
//  SNRegisterController.m
//  Vpnforios
//
//  Created by Шурик on 11.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNRegisterController.h"

#import "SNHeaderView.h"
#import "UIColor+Customization.h"
#import "SNInstallController.h"
#import "NSURLRequest+Customization.h"
#import "SVProgressHUD.h"
#import "SNConnectionManager.h"
#import "SNDashboardController.h"
#import "TTTAttributedLabel.h"

#define kMAX_PASSWORD_LENGTH 3

@import ObjectiveC.runtime;

typedef void (^AlertViewBlock)(UIAlertView *alertView, NSInteger buttonIndex);
#define kALERT_VIEW_BLOCK  @"kALERT_VIEW_BLOCK"

@interface SNRegisterController () <TTTAttributedLabelDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;
@property (weak, nonatomic) IBOutlet UITextField *refereeTextField;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *registerButton;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *footerLabel;

@end

@implementation SNRegisterController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.registerButton.enabled = NO;
    self.clearsSelectionOnViewWillAppear = YES;
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    
    NSString *footerText = NSLocalizedString(@"By clicking register you accept \nthe Terms and Conditions and Privacy Policy \nof Mobile Secure",nil);
    _footerLabel.text = footerText;
    _footerLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
    _footerLabel.delegate = self;
    NSRange rangeLink = [footerText rangeOfString:NSLocalizedString(@"Terms and Conditions",nil)];
    NSRange rangeLinkTwo = [footerText rangeOfString:NSLocalizedString(@"Privacy Policy",nil)];
    [_footerLabel addLinkToURL:[NSURL URLWithString:@"http://TermsandConditions.url"] withRange:rangeLink];
    [_footerLabel addLinkToURL:[NSURL URLWithString:@"http://PrivacyPolicy.url"] withRange:rangeLinkTwo];
}





#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    if (section == 0) {
        header.textLabel.text = [NSLocalizedString(@"Provide your email as user name, password at least 5 characters", nil) uppercaseString];
    }
    // Set the text color of our header/footer text.
//    header.textLabel.textColor = self.view.tintColor;
//    header.textLabel.text = [header.textLabel.text capitalizedString];
    
    header.textLabel.textColor = [UIColor customDarkGrayColor];
//    header.textLabel.text = [header.textLabel.text capitalizedString];
    
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.tintColor;
    header.textLabel.text = [header.textLabel.text capitalizedString];
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    
}

//#pragma mark - Table View Delegate
//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
//    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
//    header.textLabel.text = [header.textLabel.text capitalizedString];
//    header.textLabel.textColor = self.view.tintColor;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? SNTableSectionHeight : UITableViewAutomaticDimension;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didTapButtonRegister:(id)sender
{
    [self registerNewUser];
}

- (IBAction)editingChanged:(UITextField *)sender {
//    self.navigationItem.rightBarButtonItem.enabled = [self validateFieldsWithAlerts:NO];
}



#pragma mark - Text Fields Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger tag = textField.tag + 1;
    [self.textFields enumerateObjectsUsingBlock:^(UITextField *obj, NSUInteger idx, BOOL *stop) {
        if (obj.tag == tag) {
            [obj becomeFirstResponder];
            *stop = YES;
        }
    }];
    
    if (tag == [self.textFields count]) {
        [self registerNewUser];
    }
    
    return YES;
}

#pragma mark - Server Methods
- (void)registerNewUser {
    
    NSError *error = nil;
    NSString *password = [self.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *confirmPassword = [self.confirmTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *email = [self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *refree = [self.refereeTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
//    ALL FIELDS
    if ([password length] == 0 || [confirmPassword length] == 0 || [email length] == 0) {
        NSString *title = NSLocalizedString(@"Change Password", nil);
        NSString *msg = NSLocalizedString(@"Enter your current email, password and confirm your password.", nil);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return;
    }
    
//    EMAIL
    if (![SNAppDelegate isEmailValid:email error:&error]) {
        
        __weak typeof(self) weakSelf = self;
        AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.emailTextField becomeFirstResponder];
            }
        };
        
        NSString *title = NSLocalizedString(@"Change Password", nil);
        NSString *msg = [error localizedDescription];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
        [alertView show];
        
        return;
    }
    
//    Password
    if (![SNAppDelegate isPasswordValid:password error:&error]) {
        
        __weak typeof(self) weakSelf = self;
        AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.passwordTextField becomeFirstResponder];
            }
        };
        
        NSString *title = NSLocalizedString(@"Change Password", nil);
        NSString *msg = [error localizedDescription];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
        [alertView show];
        
        return;
    }
    
//    Confirm
    if (![confirmPassword isEqualToString:password]) {
        __weak typeof(self) weakSelf = self;
        AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf.confirmTextField becomeFirstResponder];
            }
        };
        
        NSString *title = NSLocalizedString(@"Change Password", nil);
        NSString *msg = NSLocalizedString(@"Password and confirmation does not match.", nil);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
        [alertView show];
        
        return;
        
    }
    
//    Referee email
    if ([refree length] > 0) {
        if (![SNAppDelegate isEmailValid:refree error:&error]) {
            
            __weak typeof(self) weakSelf = self;
            AlertViewBlock block = ^(UIAlertView *alertView, NSInteger buttonIndex) {
                typeof(weakSelf) strongSelf = weakSelf;
                if (strongSelf) {
                    [strongSelf.refereeTextField becomeFirstResponder];
                }
            };
            
            NSString *title = NSLocalizedString(@"Change Password", nil);
            NSString *msg = [error localizedDescription];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            objc_setAssociatedObject(alertView, kALERT_VIEW_BLOCK, block, OBJC_ASSOCIATION_COPY);
            [alertView show];
            
            return;
        }
    }
    
    
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:SNAppDeviceTokenKey];
    NSDictionary *dictionary = @{
                                 @"password" : password,
                                 @"email" : email,
                                 @"ref_email" : refree,
                                 @"device" : deviceToken,
                                 @"device_name" : [[UIDevice currentDevice] name]
                                 };
    NSURLRequest *request = [NSURLRequest requestWithAction:@"user.register" params:dictionary requestType:NSURLRequestTypePost];
    
    [SVProgressHUD show];
    [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
        if (error) {
            [SVProgressHUD dismiss];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
            [alertView show];
        } else {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"User registered", nil)];
            
            [[DMUser defaultUser] setEmail:self.emailTextField.text];
            [[DMUser defaultUser] setPassword:self.passwordTextField.text];
            [[DMUser defaultUser] setAccessToken:[json valueForKeyPath:@"response.access_token"]];
            [[DMUser defaultUser] setUid:[[json valueForKeyPath:@"response.uid"] integerValue]];
            
            [[DMUser defaultUser] save];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:SNAppUserDidLogin object:nil];
            
            [self performSegueWithIdentifier:NSStringFromClass([SNInstallController class]) sender:self];
        }
    }];
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(__unused TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url
{
    [[[UIActionSheet alloc] initWithTitle:[url absoluteString] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Open Link in Safari", nil), nil] showInView:self.view];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:actionSheet.title]];
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    AlertViewBlock block = objc_getAssociatedObject(alertView, kALERT_VIEW_BLOCK);
    if (block) {
        block(alertView, buttonIndex);
    }
}


@end
