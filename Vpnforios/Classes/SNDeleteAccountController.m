//
//  SNDeleteAccountController.m
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDeleteAccountController.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHeaderView.h"

#import "SNConnectionManager.h"
#import "NSURLRequest+Customization.h"
#import "SNLoginController.h"
#import "DMMessage+Category.h"

@interface SNDeleteAccountController ()

@property (weak, nonatomic) IBOutlet UIImageView *emailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;
@property (weak, nonatomic) IBOutlet UILabel *fotterLabel;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end

@implementation SNDeleteAccountController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (DEVICE_IS_IPHONE) {
        self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    }
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.clearsSelectionOnViewWillAppear = YES;
    
    self.emailTextField.placeholder = NSLocalizedString(@"Account mail", nil);
    self.passwordTextField.placeholder = NSLocalizedString(@"Confirm with your password", nil);
    [self.deleteButton setTitle:NSLocalizedString(@"Delete Account", nil) forState:UIControlStateNormal];
    
    
    [self configureView];
}

#pragma mark - Private Methods
- (void)configureView
{
    self.emailImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Email"];
    self.passwordImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Password"];
    self.fotterLabel.textColor = self.view.tintColor;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didTapButtonDelete:(id)sender
{
    NSString *title = nil;
    NSString *msg = NSLocalizedString(@"You want to delete the account?", nil);
    NSString *cancel = NSLocalizedString(@"Cancel", nil);
    NSString *delete = NSLocalizedString(@"Delete", nil);
    
    BOOL isCorrectEmail    = [self.emailTextField.text    isEqualToString:[[DMUser defaultUser] email]];
    BOOL isCorrectPassword = [self.passwordTextField.text isEqualToString:[[DMUser defaultUser] password]];
    if (isCorrectEmail && isCorrectPassword) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:cancel otherButtonTitles:delete, nil];
        [alertView show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"Email or password is not correct", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
        [alert show];
    }
}


#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.tintColor;
    
    // line below causes crash on iOS 8 beta 7
//    header.textLabel.font = [UIFont myriadFontWithSize:header.textLabel.font.pointSize];
    
    header.textLabel.text = NSLocalizedString(@"You want to delete the account", nil);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? SNTableSectionHeight : UITableViewAutomaticDimension;
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex) {
        [self deleteAccount];
    }
}

#pragma mark - Server Methods
- (void)deleteAccount
{
    [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.delete_account"] completionHandler:^(NSDictionary *json, NSError *error) {
        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
        } else {
//            NSLog(@"%@", json);
            // clear data from local data-model DMUser
            [[DMUser defaultUser] clear];
            [DMMessage removeAll];
            [self.navigationController popViewControllerAnimated:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:SNAppUserDidDeleted object:nil];
            
        }
    }];
}


@end
