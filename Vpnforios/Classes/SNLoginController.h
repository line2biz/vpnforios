//
//  SNLoginController.h
//  Vpnforios
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNLoginController : UITableViewController

@property (strong, nonatomic) NSString *uid;
@property (strong, nonatomic) NSString *access_token;

@end
