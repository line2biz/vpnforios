//
//  SNForgetPasswordController.m
//  Vpnforios
//
//  Created by Шурик on 11.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNForgetController.h"

#import "SNHeaderView.h"
#import "UIColor+Customization.h"
#import "NSURLRequest+Customization.h"
#import "SNConnectionManager.h"
#import "SVProgressHUD.h"


@interface SNForgetController ()
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (strong,nonatomic) IBOutlet UIImageView *emailImageView;

@end

@implementation SNForgetController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    [self configureView];
}

#pragma mark _ Private Methods
- (void)configureView
{
    self.emailImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Email"];
}

- (BOOL)validateFieldsWithAlerts:(BOOL)showAlerts

{
    NSString *message = nil;
    NSInteger tag = NSNotFound;
    if (self.emailTextField.text.length >= 6) {
        if (![DMUser validateEmail:self.emailTextField.text]){
            message = NSLocalizedString(@"email not valid", nil);
        }else
            return YES;
    } else {
        message = NSLocalizedString(@"email must have much characters", nil);
        tag = self.emailTextField.tag;
    }
    
    if (showAlerts && message) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alertView.tag = self.emailTextField.tag;
        [alertView show];
    }
    return NO;
}



#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex

{
    UITextField *textField = self.emailTextField;
    [textField becomeFirstResponder];
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Set the text color of our header/footer text.
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = [UIColor customDarkGrayColor];
    header.textLabel.text = [header.textLabel.text capitalizedString];
    if (section == 0) {
        header.textLabel.text = [NSLocalizedString(@"Please provide your registered email, we'll send your password to your mail box", nil) uppercaseString];
    }
    
}

//- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
//    // Set the text color of our header/footer text.
//    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
//    header.textLabel.textColor = self.view.tintColor;
//    header.textLabel.text = [header.textLabel.text capitalizedString];
//    
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? SNTableSectionHeight : UITableViewAutomaticDimension;
}

#pragma mark - Outlet Methods
- (IBAction)editingChanged:(UITextField *)sender
{
    self.navigationItem.rightBarButtonItem.enabled = [self validateFieldsWithAlerts:NO];
}

- (IBAction)didTapButtonSend:(id)sender {
    [self send];
}

#pragma mark - Server Methods

- (void)send
{
    if ([self validateFieldsWithAlerts:YES]) {
        NSDictionary *dictionary = @{@"email" : self.emailTextField.text};
        NSURLRequest *request = [NSURLRequest requestWithAction:@"user.forgot_password" params:dictionary requestType:NSURLRequestTypePost];

        [SVProgressHUD show];
        [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
            if (error) {
                [SVProgressHUD dismiss];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Cancel", nil];
                [alertView show];
            } else {
                [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Email was sent", nil)];
            }
        }];
    }
}

@end
