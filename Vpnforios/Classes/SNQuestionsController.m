//
//  SNQuiestionsController.m
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNQuestionsController.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHeaderView.h"
#import "SNConnectionManager.h"
#import "NSURLRequest+Customization.h"
#import "DMQuestion+Category.h"
#import "DMManager.h"
#import "SNQuestionController.h"

#import <SVProgressHUD/SVProgressHUD.h>

@interface SNQuestionsController () <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSIndexPath *lastIndexPath;
@property (assign, nonatomic) BOOL isFirstQuestionShown;

@end

@implementation SNQuestionsController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.managedObjectContext = [[DMManager sharedManager] defaultContext];
    
    if (DEVICE_IS_IPHONE) {
        self.navigationItem.title = NSLocalizedString(@"How to use", nil);
    } else {
        self.navigationItem.title = nil;
    }
    
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.clearsSelectionOnViewWillAppear = YES;
    [self configureView];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadFAQfromServer) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    self.isFirstQuestionShown = false;
    
    [self loadFAQfromServer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (DEVICE_IS_IPAD)
    {
        if ([self.tableView numberOfRowsInSection:0] == 0) {
            return;
        }
        
        if (!self.isFirstQuestionShown) {
            self.isFirstQuestionShown = true;
            
            self.lastIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView selectRowAtIndexPath:self.lastIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            [self performSegueWithIdentifier:NSStringFromClass([SNQuestionController class]) sender:self];
        }
        else if (self.lastIndexPath)
        {
            [self.tableView selectRowAtIndexPath:self.lastIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
}

#pragma mark - Private Methods
- (void)configureView {

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.tintColor;
//    header.textLabel.textAlignment = NSTextAlignmentCenter;
}
#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return DEVICE_IS_IPAD;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[SNQuestionController class]])
    {
        SNQuestionController *controller = [segue destinationViewController];
        controller.managedObjectContext = self.managedObjectContext;
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DMQuestion *question = [self.fetchedResultsController objectAtIndexPath:indexPath];
        controller.question = question;
    }

}

#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (DEVICE_IS_IPAD) {
        return nil;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
    DMQuestion *question = [self.fetchedResultsController objectAtIndexPath:indexPath];
    return question.body;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    DMQuestion *question = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text       = question.title;
    cell.imageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Forget"];
    cell.accessoryType = (DEVICE_IS_IPAD
                          ? UITableViewCellAccessoryDisclosureIndicator
                          : UITableViewCellAccessoryNone);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.lastIndexPath = indexPath;
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMQuestion class])];
    NSSortDescriptor *first = [NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES];
    request.sortDescriptors = @[first];
    
    NSString *sectionName = (DEVICE_IS_IPHONE ? @"identifier" : nil);
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:sectionName cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

#pragma mark - Server Methods

- (void)loadFAQfromServer {
    __weak typeof(self) weakSelf = self;
    NSString *languageID = [[NSBundle mainBundle] preferredLocalizations].firstObject;
    NSDictionary *params = @{ @"lang" : languageID,
                              };
    
    NSURLRequest *request = [NSURLRequest requestWithAction:@"faq.getList" params:params requestType:NSURLRequestTypePost];
    [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        } else {
            NSDictionary *response = [json valueForKeyPath:@"response"];
            NSManagedObjectContext *context = [[DMManager sharedManager] defaultContext];
            [DMQuestion parseResponse:response inContext:context];
        }
        
        typeof(weakSelf) strongSelf = weakSelf;
        [[strongSelf refreshControl] endRefreshing];
    }];
}

@end
