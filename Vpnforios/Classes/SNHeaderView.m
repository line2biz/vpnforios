//
//  SNHeaderView.m
//  Vpnforios
//
//  Created by Шурик on 08.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNHeaderView.h"
@interface SNHeaderView()
@property (strong,nonatomic) UIImageView *imageView;

@end;

@implementation SNHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    CGRect rect = frame;
    rect.origin = CGPointZero;
    rect.size.height = 134;
    
    self = [super initWithFrame:rect];
    if (self) {
        [self initializeComponents];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initializeComponents];
    }
    return self;
}

- (void)initializeComponents
{
    _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _imageView.contentMode  = UIViewContentModeCenter;
   
    _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.backgroundColor = [UIColor clearColor];
    [self addSubview: _imageView];
    [self configureView];
}

#pragma mark - Private Methods 
-(void)configureView
{
     self.imageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Logo-Big"];
}

- (void)tintColorDidChange
{
    [self configureView];
}
@end
