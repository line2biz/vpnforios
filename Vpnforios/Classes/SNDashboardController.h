//
//  SNDashboardController.h
//  Vpnforios
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SNDetailViewControllerIPad;

@interface SNDashboardController : UITableViewController

@property (nonatomic) SNDetailViewControllerIPad *detailViewControllerIPad;
- (void)showLoginControllerAnimated:(BOOL)animated;

@end
