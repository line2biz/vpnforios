//
//  DMDataPlan.m
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMDataPlan.h"

@implementation DMDataPlan

+ (NSString *)stringWithPlanType:(DMDataPlanType)planType
{
    NSString *str = nil;
    switch (planType) {
        case DMDataPlanTypeMonthly:
            str = NSLocalizedString(@"Monthly Non-recurring Subscription", nil);
            break;
        case DMDataPlanTypeNoExpiration:
            str = NSLocalizedString(@"Data VPN Data (No Expire Date)", nil);
            break;
    }
    return str;
}

- (NSString *)stringFromPrice
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"de_DE"]];
    return [numberFormatter stringFromNumber:self.price];
}

@end
