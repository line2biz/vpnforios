//
//  SNMessageController.m
//  Vpnforios
//
//  Created by Шурик on 09.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//


#import "SNMessageController.h"

#import "DMMessage+Category.h"
#import "SNConnectionManager.h"
#import "NSURLRequest+Customization.h"

@interface SNMessageController ()

@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation SNMessageController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
    
    if (DEVICE_IS_IPAD && self.message) {
        self.navigationItem.title = NSLocalizedString(@"Messages", nil);
    }
    
    [self updateMessageOnServer];
}


#pragma mark - Private Methods
- (void)configureView
{
    if (self.message.body) {
        NSDictionary *attributes = @{ NSFontAttributeName: self.bodyLabel.font };
        self.bodyLabel.attributedText = [[NSAttributedString alloc] initWithString:self.message.body attributes:attributes];
    }
    
    self.titleLabel.text = self.message.title;
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.message ? 2 : 0;
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        CGSize size = CGSizeMake(CGRectGetWidth(self.view.bounds) - 2 * 14, MAXFLOAT);
        
        NSDictionary *attr = @{ NSFontAttributeName : [UIFont systemFontOfSize:12] };
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:@"asfdasdf" attributes:attr];
                                                       
                                                       
                                
        
        CGRect rect = [attrStr boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin context:NULL];
        CGFloat height = ceil(CGRectGetHeight(rect)) + 2 * 11;
        return MAX(height, 44);
    }

    if (indexPath.row == 1) {
        CGSize size = CGSizeMake(CGRectGetWidth(self.view.bounds) - 2 * 14, MAXFLOAT);
        
        NSLog(@"%s self view bounds: %f", __FUNCTION__, CGRectGetWidth(self.view.bounds));
        
        NSStringDrawingOptions options = NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin;
        CGRect rect = [self.bodyLabel.attributedText boundingRectWithSize:size options:options context:NULL];
        CGFloat height = ceil(CGRectGetHeight(rect)) + 2 * 11 + 20;
        
        
        return MAX(height, 44);

    }
    
    return 44;
}

#pragma mark - Server Methods
- (void)updateMessageOnServer
{
    // Make message as read
    
    DMMessage *message = [DMMessage messageWithIdentifier:@(self.message.identifier) inContext:self.managedObjectContext];
    if (message.isUnread) {
        NSDictionary *params = @{@"news_id" : [NSString stringWithFormat:@"%lli", self.message.identifier],
                                 @"read" : @"1"};
        
        [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.setNewsRead" params:params  requestType:NSURLRequestTypePost]
                       completionHandler:^(NSDictionary *json, NSError *error) {
                           NSLog(@"%@", json);
                           DMMessage *message = [DMMessage messageWithIdentifier:@(self.message.identifier) inContext:self.managedObjectContext];
                           message.isUnread = NO;
                           [self.managedObjectContext save:nil];
                           
                           [[NSNotificationCenter defaultCenter] postNotificationName:SNAppMessageRead object:nil];
                       }];
    }
}

@end
