//
//  DMUser.h
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DMLocation;

FOUNDATION_EXTERN NSString * const DMUserSecureStatusKey;
FOUNDATION_EXTERN NSString * const DMUserLocationKey;
FOUNDATION_EXTERN NSString * const DMUserEmailKey;
FOUNDATION_EXTERN NSString * const DMUserPasswordKey;
FOUNDATION_EXTERN NSString * const DMUserRemainingDaysKey;
FOUNDATION_EXTERN NSString * const DMUserRemainingQuotasKey;
FOUNDATION_EXTERN NSString * const DMUserPermanentQuotasKey;
FOUNDATION_EXTERN NSString * const DMUserProtectionTypeKey;
FOUNDATION_EXTERN NSString * const DMUserUidKey;
FOUNDATION_EXTERN NSString * const DMUserAccessTokenKey;

FOUNDATION_EXTERN NSUInteger const DMUserPasswordMindLength;

typedef NS_ENUM (NSUInteger, SNSecureStatus) {
    SNSecureStatusNone,
    SNSecureStatusProtected,
    SNSecureStatusExpired
    
};

typedef NS_ENUM (NSUInteger, SNProtectionType) {
    SNProtectionTypeAlwaysOn,
    SNProtectionTypeManual
};

@interface DMUser : NSObject

+ (DMUser *)defaultUser;

/// TODO: Delete
//@property (assign, nonatomic) SNSecureStatus secureStatus;
//+ (NSString *)stringWithSecureStatus:(SNSecureStatus)secureStatus;

@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) DMLocation *location;
@property (assign, nonatomic) NSUInteger remainingDays;
@property (assign, nonatomic) long long remainingQuotas;
@property (assign, nonatomic) long long permanentQuotas;
@property (strong, nonatomic) NSString *accessToken;
@property (assign, nonatomic) NSUInteger uid;
@property (nonatomic) BOOL adBlock;

@property (assign, nonatomic) SNProtectionType protectionType;
+ (NSString *)stringWithProtectionType:(SNProtectionType)protectionType;

+ (BOOL)validateEmail:(NSString *)candidate;

- (void)save;
- (void)clear;
- (BOOL)isValid;

@end
