//
//  SNProtectionsController.m
//  Vpnforios
//
//  Created by Шурик on 19.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNProtectionsController.h"

#import "SNHeaderView.h"
#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNInstallController.h"

#import <SVProgressHUD/SVProgressHUD.h>
#import <NetworkExtension/NetworkExtension.h>

#import "SNVPNManager.h"

@interface SNProtectionsController () <SNVPNManagerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *footerImageView;
@property (weak, nonatomic) IBOutlet UILabel *footerLabel;

@end

@implementation SNProtectionsController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Protection", nil);
    self.clearsSelectionOnViewWillAppear = YES;
    
    if (DEVICE_IS_IPHONE) {
        self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    }
    
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    [self configureView];
    
}

#pragma mark - Private Methods
- (void)configureView
{
    self.view.tintColor = [[SNAppDelegate sharedDelegate] tintColor];
    self.footerImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Image-Protection"];
    self.footerLabel.textColor = self.view.tintColor;
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonCacel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        cell.textLabel.text = NSLocalizedString(@"Always On", nil);
    } else {
        cell.textLabel.text = NSLocalizedString(@"Manual", nil);
    }
    
//    cell.textLabel.text = [DMUser stringWithProtectionType:indexPath.row];
    
    DMUser *user = [DMUser defaultUser];
    user.protectionType = (SNProtectionType)![SNVPNManager sharedManager].alwaysON;
//    cell.accessoryType = (user.protectionType == indexPath.row) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    return cell;
    
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return;
    
    if ([DMUser defaultUser].protectionType == indexPath.row) {
        return;
    }
    
    [[DMUser defaultUser] setProtectionType:indexPath.row];
    [[DMUser defaultUser] save];
    
//    [self.tableView reloadData];
//    
//    __weak typeof(self) weakSelf = self;
    
    [SVProgressHUD show];
    
    // NEW CODE
    
    [[SNVPNManager sharedManager] setDelegate:self];
    [[SNVPNManager sharedManager] setAlwaysON:indexPath.row == 0 ? YES : NO];
    BOOL bVPNisDisconnected = ([NEVPNManager sharedManager].connection.status == NEVPNStatusDisconnected);
    [[SNVPNManager sharedManager] configureVPN_andStart:!bVPNisDisconnected];
    
}

#pragma mark - SNVPN Manager Delegate
- (void)VPNManager:(SNVPNManager *)manager savedConfigurationWithError:(NSError *)error
{
    if (!error) {
        [SVProgressHUD dismiss];
        [self.tableView reloadData];
    } else {
        NSLog(@"%s error: %@", __FUNCTION__, error);
        [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
    }
}


@end
