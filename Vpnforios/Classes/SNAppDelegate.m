//
//  SNAppDelegate.m
//  Vpnforios
//
//  Created by Шурик on 23.07.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNAppDelegate.h"
#import "DMMessage+Category.h"
#import "SNConnectionManager.h"
#import "SNDashboardController.h"
#import "Countly.h"

#import <SVProgressHUD/SVProgressHUD.h>
#import <NetworkExtension/NetworkExtension.h>


// Names for NSNotificationCenter
NSString * const SNAppUserDidLogin   = @"SNAppUserDidLogin";
NSString * const SNAppUserDidDeleted = @"SNAppUserDidDeleted";
NSString * const SNAppMessageRead    = @"SNAppMessageRead";
NSString * const SNAppDeviceTokenKey = @"SNAppDeviceTokenKey";

NSString * const SNAppTintColorKey = @"SNAppTintColorKey";

CGFloat const SNTableSectionHeight = 25.f;

@implementation SNAppDelegate

@dynamic badgeNumber;


#pragma mark - Application Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //countly
    [[Countly sharedInstance] start:@"cfa93500c2c68015160ca490277ee4418b5c4157" withHost:@"http://us12849119533871904.dynamiccloudserver.info"];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didGetUserDidLoginNotification)
                                                 name:SNAppUserDidLogin
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didExpireAccessToken:) name:SNConnectionManagerInvalidAccessTokenKey object:nil];
    
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

    NEVPNManager *manager = [NEVPNManager sharedManager];
    [manager loadFromPreferencesWithCompletionHandler:^(NSError *error) {
        if(error) {
            NSLog(@"%s Load error: %@", __FUNCTION__,  error);
        }
    }];
    
    
//    [[DMUser defaultUser] setSecureStatus:SNSecureStatusNone];
    
    self.appearanceColor = (SNAppearanceColor)[[NSUserDefaults standardUserDefaults] integerForKey:SNAppTintColorKey];
    [SNAppearance customizeApprarance];
    
    
    
    
    if ( DEVICE_IS_IPAD ) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        
        NSArray *navigationControllers = @[@"SNDashboardNavigationController",
                                           @"SNDataPlansNavigationController",
                                           @"SNMessagesNavigationController",
                                           @"SNQuiestionsNavigationController"];
        
        NSMutableArray *splitControllers = [NSMutableArray new];
        
        UIBarButtonItem *logout = nil;
        
        for (NSString *controller in navigationControllers)
        {
            UISplitViewController *split = [[UISplitViewController alloc] init];
            
            UINavigationController *master = [storyboard instantiateViewControllerWithIdentifier:controller];
            
            UIViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"SNMessageController"];
            UINavigationController *detail = [[UINavigationController alloc] initWithRootViewController:detailView];
            
            
            if (!logout) {
                logout = ((UINavigationItem *)((UITableViewController *)master.viewControllers[0]).navigationItem).leftBarButtonItem;
            }
            
//            ((UINavigationItem *)((UITableViewController *)detail.viewControllers[0]).navigationItem).title = ((UINavigationItem *)((UITableViewController *)master.viewControllers[0]).navigationItem).title;
            if (logout) {
                ((UINavigationItem *)((UITableViewController *)master.viewControllers[0]).navigationItem).leftBarButtonItem = logout;
            }
            
                
            split.viewControllers = @[master, detail];
            split.delegate = self;
        
            split.tabBarItem = master.tabBarItem;
            [splitControllers addObject:split];
        }
        
        
        
        UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
        tabBarController.viewControllers = splitControllers;
        
        self.tabBarController = tabBarController;
    }
    
    
    UIViewController *controller = self.window.rootViewController;
    for (UINavigationController *navController in controller.childViewControllers) {
        for (UIViewController *childController in navController.childViewControllers) {
            if ([childController conformsToProtocol:@protocol(SNAppDelegateStartupProtocol)]) {
                [childController performSelector:@selector(applicationDidStart)];
            }
        }
    }
    
    
    NSInteger badgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber];
    self.badgeNumber = badgeNumber;
    
    return YES;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didGetUserDidLoginNotification {
    
    // load messages from server after user logged in
    UIViewController *controller = self.window.rootViewController;
    
    if (DEVICE_IS_IPHONE) {
        for (UINavigationController *navController in controller.childViewControllers) {
            for (UIViewController *childController in navController.childViewControllers) {
                if ([childController conformsToProtocol:@protocol(SNAppDelegateStartupProtocol)]) {
                    [childController performSelector:@selector(applicationDidStart)];
                }
            }
        }
    } else {
        for (UISplitViewController *splitViewController in controller.childViewControllers) {
            for (UINavigationController *navController in splitViewController.childViewControllers) {
                for (UIViewController *childController in navController.childViewControllers) {
                    if ([childController conformsToProtocol:@protocol(SNAppDelegateStartupProtocol)]) {
                        [childController performSelector:@selector(applicationDidStart)];
                    }
                }
            }
        }
    }
    
}


#pragma mark - Split View Controller Delegate
- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation {
    return NO;
}


#pragma mark - APNS Integration
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
#if !TARGET_IPHONE_SIMULATOR
    
    NSLog(@"Error in APNS registration. Error: %@", error);
#else
    NSString *deviceToken = @"12668fc8b01e63d0f2b3452a7d2196dd8c52235a36dce3e5909b432cb70c3334";
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:SNAppDeviceTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
#endif
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *newDeviceToken = [deviceToken description];
    newDeviceToken = [newDeviceToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newDeviceToken = [newDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    // Save the token to server
//    NSLog(@"%s deviceToken: %@", __FUNCTION__, newDeviceToken);
    
    [[NSUserDefaults standardUserDefaults] setObject:newDeviceToken forKey:SNAppDeviceTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    NSString *message = apsInfo[@"type"];
    NSLog(@"%s %@", __FUNCTION__, message);
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    UIViewController *controller = self.window.rootViewController;
    
    if (DEVICE_IS_IPHONE) {
        for (UINavigationController *navController in controller.childViewControllers) {
            for (UIViewController *childController in navController.childViewControllers) {
                if ([childController conformsToProtocol:@protocol(SNAppDelegateForegroundProtocol)]) {
                    [childController performSelector:@selector(applicationWillAppearInForeground)];
                }
            }
        }
    }
    else {
        for (UISplitViewController *splitViewController in controller.childViewControllers) {
            for (UINavigationController *navController in splitViewController.childViewControllers) {
                for (UIViewController *childController in navController.childViewControllers) {
                    if ([childController conformsToProtocol:@protocol(SNAppDelegateForegroundProtocol)]) {
                        [childController performSelector:@selector(applicationWillAppearInForeground)];
                    }
                }
            }
        }
    }
}


#pragma mark - Public Class Methods
- (UIColor *)tintColor
{
    return self.window.tintColor;
}

+ (SNAppDelegate *)sharedDelegate
{
    return (SNAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (UIImage *)imageWithImageName:(NSString *)imageName
{
    if (self.appearanceColor == SNAppearanceColorRed) {
        imageName = [imageName stringByAppendingString:@"-Red"];
    } else {
        imageName = [imageName stringByAppendingString:@"-Blue"];
    }
    
    return [UIImage imageNamed:imageName];
}



#pragma mark - Property accessors
- (void)setAppearanceColor:(SNAppearanceColor)appearanceColor
{
    _appearanceColor = appearanceColor;
    if (_appearanceColor==SNAppearanceColorRed) {
        self.window.tintColor = [UIColor colorWithRed:255/255.f green:48/255.f blue:98/255.f alpha:1];
    } else {
        self.window.tintColor = [UIColor colorWithRed:0/255.f green:122/255.f blue:255/255.f alpha:1];
    }
}


- (NSUInteger)badgeNumber
{
    return [[UIApplication sharedApplication] applicationIconBadgeNumber];
}

- (void)setBadgeNumber:(NSUInteger)badgeNumber
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = badgeNumber;
    
    UITabBarItem *tabBarItem = nil;
    
    if ([self.window.rootViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
        tabBarItem = tabBarController.tabBar.items[2];
    }
    // for iPad version
    else if ([self.window.rootViewController isKindOfClass:[UISplitViewController class]])
    {
        UISplitViewController *svc = (UISplitViewController *)self.window.rootViewController;
        UITabBarController *tabBarController = svc.viewControllers[0];
        
        if (tabBarController.tabBar.items.count > 2)
        {
            tabBarItem = tabBarController.tabBar.items[2];
        }
        
    }
    
    if (tabBarItem != nil) {
        tabBarItem.badgeValue = (badgeNumber > 0)
                                ? [NSString stringWithFormat:@"%zd", badgeNumber]
                                : nil;
    }
}

#pragma mark - Password Validation
+ (BOOL)isPasswordValid:(NSString *)pwd error:(NSError **)error {
    
    BOOL valid = YES;
    
    if ( [pwd length]<6 || [pwd length]>20 ) {
        valid = NO;
    }
    
    NSRange rang;
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    
    if ( !rang.length )
        valid = NO;  // no letter
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if ( !rang.length )
        valid = NO;  // no number;
    
    if (!valid) {
        
        NSInteger code = 1550000;
        NSString *description = NSLocalizedString(@"Please choose a password which is at least 6 characters long and consists of numbers and letters.", nil);
        *error = [NSError errorWithDomain:@"vpnforios" code:code userInfo:[NSDictionary dictionaryWithObject:description forKey:NSLocalizedDescriptionKey]];
        
        return NO;
    } else {
        return YES;
    }
    
}

+ (BOOL)isEmailValid:(NSString *)emil error:(NSError **)error {
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if ([emailTest evaluateWithObject:emil]) {
        return YES;
    } else {
        NSInteger code = 1440000;
        NSString *description = NSLocalizedString(@"Please enter valid email", nil);
        *error = [NSError errorWithDomain:@"vpnforios" code:code userInfo:[NSDictionary dictionaryWithObject:description forKey:NSLocalizedDescriptionKey]];
        return NO;
    }
}

- (void)didExpireAccessToken:(NSNotification *)notification {
    
    NSLog(@"%s object: %@ %@", __FUNCTION__, [notification object], NSStringFromClass([[notification object] class]));
    
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
    
    NSError *error = [notification object];
    
    if ([[DMUser defaultUser] isValid]) {
        [[DMUser defaultUser] clear];
        [[DMUser defaultUser] save];
        NSString *title = NSLocalizedString(@"", nil);
        NSString *msg = error.localizedDescription;
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alerView show];
    }
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    [tabBarController setSelectedIndex:0];
    
    UINavigationController *navController = (UINavigationController *)tabBarController.viewControllers[0];
    SNDashboardController *dashboardController = (SNDashboardController *)navController.viewControllers[0];
    
    
    [dashboardController showLoginControllerAnimated:YES];
    
}


@end

float IOSVersion() {
    NSString *iOSversion = [[UIDevice currentDevice] systemVersion];
    NSString *prefix = [[iOSversion componentsSeparatedByString:@"."] firstObject];
    float versionVal = [prefix floatValue];
    
//    return 7.f;
    return versionVal;
}
