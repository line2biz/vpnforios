//
//  SNStatisticsController.m
//  Vpnforios
//
//  Created by Шурик on 03.12.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNStatisticsController.h"

#import "SNAppearance.h"
#import "SNHeaderView.h"
#import "UIColor+Customization.h"
#import "SNConnectionManager.h"
#import "NSURLRequest+Customization.h"
#import "SNStatisticsCell.h"


#import <SVProgressHUD/SVProgressHUD.h>

#define kKEY_DURATION   @"duration"
#define kKEY_START      @"start"
#define kKEY_VALUE      @"used"

@interface SNStatisticsController ()


@property (strong, nonatomic) NSArray *items;

@end

@implementation SNStatisticsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"Statistics", nil);
    self.clearsSelectionOnViewWillAppear = NO;
    
    if (DEVICE_IS_IPHONE) {
        self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
    }
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    
    [self loadDataFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Outlet Methods
- (IBAction)didTapButtonRefresh:(id)sender {
    [self loadDataFromServer];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SNStatisticsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dictionary = self.items[indexPath.row];
    
    
    NSString *sDate = dictionary[kKEY_START];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:sDate];
    
    NSLog(@"\ndate %@\nsdat %@\n\n", date, sDate);
    
    dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"yyyy-MM-dd HH:mm:ss" options:0 locale:[NSLocale currentLocale]];
    cell.dateLabel.text = [dateFormatter stringFromDate:date];
    
    NSInteger interval = [dictionary[kKEY_DURATION] integerValue];
    NSInteger minutes = (NSInteger)floorf(interval / 60.f);
    NSInteger seconds = interval - minutes * 60;
    cell.intervalLabel.text = [NSString stringWithFormat:@"%02zd m: %02zd s", minutes, seconds];
    
    long long val = [dictionary[kKEY_VALUE] longLongValue];
    cell.valueLabel.text = [NSByteCountFormatter stringFromByteCount:val countStyle:NSByteCountFormatterCountStyleFile];
    return cell;
}

#pragma mark - Server Methods
- (void)loadDataFromServer {
    [SVProgressHUD show];
    __weak typeof(self) weakSelf = self;
    DMUser *user = [DMUser defaultUser];
    if (user.uid) {
        
        [SNConnectionManager sendRequest:[NSURLRequest requestWithAction:@"user.statistics" params:nil requestType:NSURLRequestTypeGet] completionHandler:^(NSDictionary *json, NSError *error) {
            
            [SVProgressHUD dismiss];
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                if (error) {
                    [SVProgressHUD dismiss];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                } else {
                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:kKEY_START ascending:NO];
                    strongSelf.items = [json[@"response"] sortedArrayUsingDescriptors:@[sort]];
                    [strongSelf.tableView reloadData];
                    
                    NSLog(@"%s %@", __FUNCTION__, json);
                }
            }
        }];
    }
}

@end
