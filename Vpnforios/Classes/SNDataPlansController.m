//
//  SNDataPlansController.m
//  Vpnforios
//
//  Created by Шурик on 20.08.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNDataPlansController.h"

#import "UIColor+Customization.h"
#import "UIFont+Customization.h"
#import "SNHeaderView.h"
#import "DMDataPlan.h"
#import "SNDataPlanCell.h"
#import "SNDataPlanController.h"
#import "SNStoreManager.h"
#import "NSURLRequest+Customization.h"
#import "SNConnectionManager.h"

#import <SVProgressHUD/SVProgressHUD.h>

@import StoreKit;


@interface SNDataPlansController () <SKProductsRequestDelegate>

@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) SKProductsRequest *productsRequest;

@property (strong,nonatomic) IBOutlet UIImageView *quota;

@end

@implementation SNDataPlansController

#pragma mark - Initialization
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.navigationController.tabBarItem.title = NSLocalizedString(@"Data Plans", nil);
    }
    return self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (DEVICE_IS_IPHONE) {
        self.navigationItem.title = NSLocalizedString(@"Data Plans", nil);
    } else {
        self.navigationItem.title = nil;
    }
    
    self.tableView.tableHeaderView = [[SNHeaderView alloc] initWithFrame:self.view.bounds];
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 44;
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.clearsSelectionOnViewWillAppear = YES;
    
    [self loadDataFromServer];
    
    
//    
//    DMDataPlan *plan1 = [DMDataPlan new];
//    plan1.title = @"30 Days Standard Plan";
//    plan1.specification = @"30 Days , 4GB data transfer";
//    plan1.price = @(1.79);
//    plan1.planType = DMDataPlanTypeMonthly;
//    
//    DMDataPlan *plan2 = [DMDataPlan new];
//    plan2.title = @"60 Days Standard Plan";
//    plan2.specification = @"60 Days , 4GB data transfer";
//    plan2.price = @(3.79);
//    plan2.planType = DMDataPlanTypeNoExpiration;
//    
//    DMDataPlan *plan3 = [DMDataPlan new];
//    plan3.title = @"90 Days Standard Plan";
//    plan3.specification = @"90 Days , 4GB data transfer";
//    plan3.price = @(6.79);
//    plan3.planType = DMDataPlanTypeNoExpiration;
//    
//    self.items = @[@[plan1], @[plan2, plan3]];
//    
//    [SNStoreManager sharedManager];
    
    [self configureView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
}

#pragma mark - Private Methods
- (void)configureView
{
    self.view.tintColor = [[SNAppDelegate sharedDelegate] tintColor];
    self.quota.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Quota"];
    
}

- (DMDataPlan *)planWithIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr = self.items[indexPath.section];
    return arr[indexPath.row];
}

- (SKProduct *)productForIndexPath:(NSIndexPath *)indexPath {
    NSArray *array = self.items[indexPath.section];
    return array[indexPath.row];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [[segue destinationViewController] setProduct:[self productForIndexPath:indexPath]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.items count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [DMDataPlan stringWithPlanType:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items[section] count];
}

- (void)configureCell:(SNDataPlanCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        cell.titleLabel.numberOfLines = 1;
        cell.subtitleLabel.numberOfLines = 1;
    }
    else {
        cell.titleLabel.numberOfLines = 0;
        cell.subtitleLabel.numberOfLines = 0;
    }
    
    
    SKProduct *product = [self productForIndexPath:indexPath];
    cell.titleLabel.text = product.localizedTitle;
    cell.subtitleLabel.text = product.localizedDescription;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    cell.priceLabel.text = [numberFormatter stringFromNumber:product.price];
    
    //    DMDataPlan *plan = [self planWithIndexPath:indexPath];
    //    cell.titleLabel.text = plan.title;
    //    cell.subtitleLabel.text = plan.specification;
    //    cell.priceLabel.text = [plan stringFromPrice];
    
//    cell.iconImageView.image = [[SNAppDelegate sharedDelegate] imageWithImageName:@"Icon-Quota"];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SNDataPlanCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
}


#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text = [DMDataPlan stringWithPlanType:section];
    header.textLabel.textColor = self.view.tintColor;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0) ? SNTableSectionHeight : UITableViewAutomaticDimension;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if (section == 0) {
//        return 40;
//    }
//    else {
//        return -1;
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    if (indexPath.section == 1) {
        SNDataPlanCell *cell = (SNDataPlanCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        
        [cell.titleLabel sizeToFit];
        [cell.subtitleLabel sizeToFit];
        
        return cell.titleLabel.frame.size.height + cell.subtitleLabel.frame.size.height + 2*8 + 1;
    }
    else return 47;
}


#pragma mark - Server Methods
- (void)loadDataFromServer {
    
    [SVProgressHUD show];
    
    __weak typeof(self) weakSelf = self;
    
    NSURLRequest *request = [NSURLRequest requestWithAction:@"billing.plans" params:nil requestType:NSURLRequestTypePost];
    [SNConnectionManager sendRequest:request completionHandler:^(NSDictionary *json, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:[error localizedDescription]];
        } else {
            
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                NSSet *set = [NSSet setWithArray:[json[@"response"] valueForKeyPath:@"planId"]];
                strongSelf.productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
                strongSelf.productsRequest.delegate = self;
                [strongSelf.productsRequest start];
                
            }
        }
    }];
}

#pragma mark - SKProductsRequestDelegate methods
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
//    NSArray *products = response.products;
    
    
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES];
    NSArray *array = [response.products sortedArrayUsingDescriptors:@[sortDesc]];
    
    NSMutableArray *daysArray = [NSMutableArray new];
    NSMutableArray *valueArray = [NSMutableArray new];
    
    for (SKProduct *product in array) {
        NSString *identifier = product.productIdentifier;
        if ([[identifier substringFromIndex:(identifier.length - 4)] isEqualToString:@"days"]) {
            [daysArray addObject:product];
        } else {
            [valueArray addObject:product];
        }
    }
    
    self.items = @[ daysArray, valueArray ];
    
    [self.tableView reloadData];
    
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
    
//    for (SKProduct *product in products) {
//        NSLog(@"%s %@", __FUNCTION__, product);
//    }
//    
    for (NSString *invalidProductId in response.invalidProductIdentifiers) {
        NSLog(@"Invalid product id: %@" , invalidProductId);
    }
    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    if ([SVProgressHUD isVisible]) {
        [SVProgressHUD dismiss];
    }
    NSString *title = NSLocalizedString(@"Failed to load list of products.", nil);
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:title message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [message show];
    self.productsRequest = nil;
}

@end
