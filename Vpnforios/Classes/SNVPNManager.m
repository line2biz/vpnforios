//
//  SNVPNManager.m
//  Vpnforios
//
//  Created by Denis on 14.03.15.
//  Copyright (c) 2015 Alexandr Zhovty. All rights reserved.
//

#import "SNVPNManager.h"

#import <NetworkExtension/NetworkExtension.h>
#import <Security/Security.h>

static NSString *serviceName = @"com.mycompany.myAppServiceName";

@interface SNVPNManager ()

@end

@implementation SNVPNManager

#pragma mark - Life Cycle
+ (instancetype)sharedManager
{
    static SNVPNManager *vpnManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        vpnManager = [[self alloc] init];
    });
    return vpnManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self savePassword:[[DMUser defaultUser] password] forKey:@"password"];
        [self savePassword:@"vpn" forKey:@"sharedSecret"];
    }
    return self;
}

#pragma mark - Private Methods
- (void)savePassword:(NSString *)password forKey:(NSString *)key
{
    NSData *data = [password dataUsingEncoding:NSUTF8StringEncoding]; // Data to save. It can be a string too.
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    NSData *encodedKey = [key dataUsingEncoding:NSUTF8StringEncoding];
    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrGeneric];
    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrAccount];
    [dict setObject:serviceName forKey:(__bridge id)kSecAttrService];
    [dict setObject:(__bridge id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];
    [dict setObject:data forKey:(__bridge id)kSecValueData];
    
    // If the keychain item already exists, delete it:
    CFDictionaryRef attributes = nil;
    if (SecItemCopyMatching((__bridge CFDictionaryRef)dict, (CFTypeRef *)&attributes) == noErr) {
        OSStatus errorcode = SecItemDelete((__bridge CFDictionaryRef)dict);
        NSAssert(errorcode == noErr, @"Couldn't update the Keychain Item." );
        
        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dict, NULL);
        if(errSecSuccess != status) {
            NSLog(@"Unable add item! error:%d",(int)status);
        }
    } else {
        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dict, NULL);
        if(errSecSuccess != status) {
            NSLog(@"Unable add item! error:%d",(int)status);
        }
    }
    
}

- (NSData *)passwordForKey:(NSString *)key
{
    NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
    [dict setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    NSData *encodedKey = [key dataUsingEncoding:NSUTF8StringEncoding];
    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrGeneric];
    [dict setObject:encodedKey forKey:(__bridge id)kSecAttrAccount];
    [dict setObject:serviceName forKey:(__bridge id)kSecAttrService];
    [dict setObject:(__bridge id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];
    [dict setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [dict setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnPersistentRef]; // The most important part
    
    CFTypeRef result = NULL;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)dict,&result);
    
    if( status != errSecSuccess) {
        NSLog(@"\n%s\nUnable to fetch item! error:%d",__PRETTY_FUNCTION__,(int)status);
        return nil;
    }
    
    NSData *resultData = (__bridge NSData *)result; //Your data is ready
    return resultData;
}

#pragma mark - Public Methods
- (void)configureVPN_andStart:(BOOL)start
{
    [self savePassword:[[DMUser defaultUser] password] forKey:@"password"];
    [self savePassword:@"vpn" forKey:@"sharedSecret"];
    NEVPNManager *manager = [NEVPNManager sharedManager];
    [manager loadFromPreferencesWithCompletionHandler:^(NSError *error) {
        if(error) {
            NSLog(@"Load error: %@", error);
        } else {
            [manager setEnabled:YES];
            
            // No errors! The rest of your codes goes here...
            NEVPNProtocolIPSec *p = [[NEVPNProtocolIPSec alloc] init];
            p.username = [[DMUser defaultUser] email];
            p.passwordReference = [self passwordForKey:@"password"];
            p.serverAddress = @"82.220.38.128";
            p.authenticationMethod = NEVPNIKEAuthenticationMethodSharedSecret;
            p.sharedSecretReference = [self passwordForKey:@"sharedSecret"];
            p.localIdentifier = @"10.0.1.3";
            p.remoteIdentifier = @"vpn";
            p.useExtendedAuthentication = YES;
            p.disconnectOnSleep = NO;
            
            [manager setProtocol:p];
            
            if (self.alwaysON) {
                NSMutableArray *rules = [[NSMutableArray alloc] init];
                NEOnDemandRuleConnect *connectRule = [NEOnDemandRuleConnect new];
                [rules addObject:connectRule];
                [manager setOnDemandRules:rules];
                
                [manager setOnDemandEnabled:YES];
            } else {
                [manager setOnDemandEnabled:NO];
            }
            
            [manager setLocalizedDescription:@"Configures VPN settings, including authentication."];
            [manager saveToPreferencesWithCompletionHandler:^(NSError *error) {
                
                if (self.delegate) {
                    if ([self.delegate respondsToSelector:@selector(VPNManager:savedConfigurationWithError:)]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.delegate VPNManager:self savedConfigurationWithError:error];
                        });
                    }
                }
                
                if(error) {
                    NSLog(@"Save error: %@", error);
                }
                else {
                    NSLog(@"Saved!");
                    if (start) {
                        NSError *startError;
                        BOOL flag = [[NEVPNManager sharedManager].connection startVPNTunnelAndReturnError:&startError];
                        
                        if (self.delegate) {
                            if ([self.delegate respondsToSelector:@selector(VPNManager:startedConnectionWithError:)]) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.delegate VPNManager:self startedConnectionWithError:startError];
                                });
                            }
                        }
                        
                        NSLog(@"%s FLAG :%zd", __FUNCTION__, flag);
                        if (startError) {
                            
                            NSLog(@"Start VPN failed: [%@]", startError.localizedDescription);
                        }
                    }
                }
            }];
        }
    }];
}

#pragma mark - Property Accessors
- (void)setDelegate:(id<SNVPNManagerDelegate>)delegate
{
    if ([delegate conformsToProtocol:@protocol(SNVPNManagerDelegate)]) {
        _delegate = delegate;
    } else {
        NSLog(@"\n\n%s\nDelegate doesn`t conform to protocol %@!\n\n", __PRETTY_FUNCTION__, NSStringFromProtocol(@protocol(SNVPNManagerDelegate)));
    }
}

- (void)setAlwaysON:(BOOL)alwaysON
{
    _alwaysON = alwaysON;
}

@end
