//
//  DMMessage+Category.h
//  Vpnforios
//
//  Created by Andrey Serebryakov on 08.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "DMMessage.h"

@interface DMMessage (Category)

+ (DMMessage *)messageWithIdentifier:(NSNumber *)identifier inContext:(NSManagedObjectContext *)context;
+ (void)parseResponseArray:(NSArray *)entities inContext:(NSManagedObjectContext *)context;
+ (void)removeAll;
+ (NSUInteger)unreadMessagesCount;

@end
