//
//  SNQuestionController.m
//  Vpnforios
//
//  Created by Andrey Serebryakov on 13.09.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNQuestionController.h"
#import "DMQuestion+Category.h"

@interface SNQuestionController ()
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation SNQuestionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (DEVICE_IS_IPAD) {
        self.navigationItem.title = NSLocalizedString(@"How to use", nil);
    }
    
    self.textLabel.text = self.question.body;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize size = CGSizeMake(CGRectGetWidth(self.view.bounds) - 2 * 14, MAXFLOAT);
    CGRect rect = [self.textLabel.attributedText boundingRectWithSize:size
                                                              options:NSStringDrawingTruncatesLastVisibleLine
                                                                     |NSStringDrawingUsesFontLeading
                                                                     |NSStringDrawingTruncatesLastVisibleLine
                                                                     |NSStringDrawingUsesLineFragmentOrigin
                                                              context:NULL];
    CGFloat height = ceil(CGRectGetHeight(rect)) + 2 * 11;
    return MAX(height, 44);
}


@end
