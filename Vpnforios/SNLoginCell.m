//
//  SNLoginCell.m
//  Vpnforios
//
//  Created by Admin on 11/24/14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNLoginCell.h"

@implementation SNLoginCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (DEVICE_IS_IPAD) {
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x+5, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height);
    }else {
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x+1, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height);
    }
}
@end
